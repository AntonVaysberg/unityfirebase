﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StartUp
struct  StartUp_t1662321771  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject StartUp::text
	GameObject_t1756533147 * ___text_2;
	// System.Boolean StartUp::tokenSent
	bool ___tokenSent_3;

public:
	inline static int32_t get_offset_of_text_2() { return static_cast<int32_t>(offsetof(StartUp_t1662321771, ___text_2)); }
	inline GameObject_t1756533147 * get_text_2() const { return ___text_2; }
	inline GameObject_t1756533147 ** get_address_of_text_2() { return &___text_2; }
	inline void set_text_2(GameObject_t1756533147 * value)
	{
		___text_2 = value;
		Il2CppCodeGenWriteBarrier(&___text_2, value);
	}

	inline static int32_t get_offset_of_tokenSent_3() { return static_cast<int32_t>(offsetof(StartUp_t1662321771, ___tokenSent_3)); }
	inline bool get_tokenSent_3() const { return ___tokenSent_3; }
	inline bool* get_address_of_tokenSent_3() { return &___tokenSent_3; }
	inline void set_tokenSent_3(bool value)
	{
		___tokenSent_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
