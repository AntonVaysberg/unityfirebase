﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp_U3CModuleU3E3783534214.h"
#include "AssemblyU2DCSharp_AF_Sample_BGScript3397449209.h"
#include "mscorlib_System_Void1841601450.h"
#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "UnityEngine_UnityEngine_Coroutine2299508840.h"
#include "AssemblyU2DCSharp_AF_Sample_BGScript_U3CPressAnimU3813865769.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_UInt322149682021.h"
#include "mscorlib_System_Int322071877448.h"
#include "UnityEngine_UI_UnityEngine_UI_Text356221433.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_UnityEngine_WaitForSeconds3839502067.h"
#include "mscorlib_System_Single2076509932.h"
#include "mscorlib_System_NotSupportedException1793819818.h"
#include "AssemblyU2DCSharp_StartUp1662321771.h"
#include "UnityEngine_UnityEngine_ScreenOrientation4019489636.h"
#include "UnityEngine_UnityEngine_Object1021602117.h"
#include "UnityEngine_UnityEngine_iOS_NotificationType3745088907.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3943999495.h"
#include "mscorlib_System_Text_Encoding663144255.h"
#include "mscorlib_System_Byte3683104436.h"
#include "UnityEngine_UnityEngine_KeyCode2283395152.h"

// AF_Sample_BGScript
struct AF_Sample_BGScript_t3397449209;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t1158329972;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// UnityEngine.Coroutine
struct Coroutine_t2299508840;
// AF_Sample_BGScript/<PressAnim>c__Iterator0
struct U3CPressAnimU3Ec__Iterator0_t3813865769;
// System.Object
struct Il2CppObject;
// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.WaitForSeconds
struct WaitForSeconds_t3839502067;
// System.NotSupportedException
struct NotSupportedException_t1793819818;
// StartUp
struct StartUp_t1662321771;
// UnityEngine.Object
struct Object_t1021602117;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t3943999495;
// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct Dictionary_2_t2281509423;
// System.String
struct String_t;
// System.Text.Encoding
struct Encoding_t663144255;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
extern Il2CppClass* AF_Sample_BGScript_t3397449209_il2cpp_TypeInfo_var;
extern const uint32_t AF_Sample_BGScript_Update_m1473604093_MetadataUsageId;
extern const uint32_t AF_Sample_BGScript_pressed_m2182290796_MetadataUsageId;
extern Il2CppClass* U3CPressAnimU3Ec__Iterator0_t3813865769_il2cpp_TypeInfo_var;
extern const uint32_t AF_Sample_BGScript_PressAnim_m2772021084_MetadataUsageId;
extern const uint32_t AF_Sample_BGScript__cctor_m3276419787_MetadataUsageId;
extern Il2CppClass* WaitForSeconds_t3839502067_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisText_t356221433_m1217399699_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1548508307;
extern Il2CppCodeGenString* _stringLiteral3229045867;
extern const uint32_t U3CPressAnimU3Ec__Iterator0_MoveNext_m1503219590_MetadataUsageId;
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CPressAnimU3Ec__Iterator0_Reset_m2688908769_MetadataUsageId;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t3943999495_il2cpp_TypeInfo_var;
extern Il2CppClass* Encoding_t663144255_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m28427054_MethodInfo_var;
extern const MethodInfo* Dictionary_2_Add_m3686231158_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3423761286;
extern Il2CppCodeGenString* _stringLiteral2801716075;
extern Il2CppCodeGenString* _stringLiteral2801716078;
extern Il2CppCodeGenString* _stringLiteral3259574281;
extern Il2CppCodeGenString* _stringLiteral1348778707;
extern Il2CppCodeGenString* _stringLiteral337074973;
extern Il2CppCodeGenString* _stringLiteral884246882;
extern Il2CppCodeGenString* _stringLiteral1881721028;
extern Il2CppCodeGenString* _stringLiteral98844765;
extern Il2CppCodeGenString* _stringLiteral1690816450;
extern Il2CppCodeGenString* _stringLiteral3035521982;
extern Il2CppCodeGenString* _stringLiteral621716435;
extern const uint32_t StartUp_Start_m1623301020_MetadataUsageId;
extern Il2CppClass* Input_t1785128008_il2cpp_TypeInfo_var;
extern const uint32_t StartUp_Update_m1673313907_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2052050410;
extern Il2CppCodeGenString* _stringLiteral104526608;
extern Il2CppCodeGenString* _stringLiteral3232720084;
extern Il2CppCodeGenString* _stringLiteral3941577958;
extern Il2CppCodeGenString* _stringLiteral1598873091;
extern Il2CppCodeGenString* _stringLiteral3686229262;
extern Il2CppCodeGenString* _stringLiteral3344737995;
extern const uint32_t StartUp_Purchase_m4261947645_MetadataUsageId;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1259047537;
extern Il2CppCodeGenString* _stringLiteral1502599639;
extern Il2CppCodeGenString* _stringLiteral607539246;
extern Il2CppCodeGenString* _stringLiteral1824391588;
extern const uint32_t StartUp_didReceiveConversionData_m3983156178_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1294667269;
extern const uint32_t StartUp_didReceiveConversionDataWithError_m2120681874_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral184702546;
extern const uint32_t StartUp_onAppOpenAttribution_m1833310285_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral3688921710;
extern const uint32_t StartUp_onAppOpenAttributionFailure_m2441954989_MetadataUsageId;

// System.Byte[]
struct ByteU5BU5D_t3397334013  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) uint8_t m_Items[1];

public:
	inline uint8_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline uint8_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, uint8_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline uint8_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline uint8_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, uint8_t value)
	{
		m_Items[index] = value;
	}
};


// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared (GameObject_t1756533147 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::.ctor()
extern "C"  void Dictionary_2__ctor_m584589095_gshared (Dictionary_2_t2281509423 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::Add(!0,!1)
extern "C"  void Dictionary_2_Add_m4209421183_gshared (Dictionary_2_t2281509423 * __this, Il2CppObject * p0, Il2CppObject * p1, const MethodInfo* method);

// System.Void UnityEngine.MonoBehaviour::.ctor()
extern "C"  void MonoBehaviour__ctor_m2464341955 (MonoBehaviour_t1158329972 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GameObject::SetActive(System.Boolean)
extern "C"  void GameObject_SetActive_m2887581199 (GameObject_t1756533147 * __this, bool p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator AF_Sample_BGScript::PressAnim()
extern "C"  Il2CppObject * AF_Sample_BGScript_PressAnim_m2772021084 (AF_Sample_BGScript_t3397449209 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.Collections.IEnumerator)
extern "C"  Coroutine_t2299508840 * MonoBehaviour_StartCoroutine_m2470621050 (MonoBehaviour_t1158329972 * __this, Il2CppObject * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AF_Sample_BGScript/<PressAnim>c__Iterator0::.ctor()
extern "C"  void U3CPressAnimU3Ec__Iterator0__ctor_m727930690 (U3CPressAnimU3Ec__Iterator0_t3813865769 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Object::.ctor()
extern "C"  void Object__ctor_m2551263788 (Il2CppObject * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.UI.Text>()
#define GameObject_GetComponent_TisText_t356221433_m1217399699(__this, method) ((  Text_t356221433 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// System.Void UnityEngine.WaitForSeconds::.ctor(System.Single)
extern "C"  void WaitForSeconds__ctor_m1990515539 (WaitForSeconds_t3839502067 * __this, float p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.NotSupportedException::.ctor()
extern "C"  void NotSupportedException__ctor_m3232764727 (NotSupportedException_t1793819818 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Application::set_runInBackground(System.Boolean)
extern "C"  void Application_set_runInBackground_m3543179741 (Il2CppObject * __this /* static, unused */, bool p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Screen::set_orientation(UnityEngine.ScreenOrientation)
extern "C"  void Screen_set_orientation_m413080199 (Il2CppObject * __this /* static, unused */, int32_t p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Object::DontDestroyOnLoad(UnityEngine.Object)
extern "C"  void Object_DontDestroyOnLoad_m2330762974 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.String>::.ctor()
#define Dictionary_2__ctor_m28427054(__this, method) ((  void (*) (Dictionary_2_t3943999495 *, const MethodInfo*))Dictionary_2__ctor_m584589095_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.String>::Add(!0,!1)
#define Dictionary_2_Add_m3686231158(__this, p0, p1, method) ((  void (*) (Dictionary_2_t3943999495 *, String_t*, String_t*, const MethodInfo*))Dictionary_2_Add_m4209421183_gshared)(__this, p0, p1, method)
// System.Void AppsFlyer::setAppsFlyerKey(System.String)
extern "C"  void AppsFlyer_setAppsFlyerKey_m1791358164 (Il2CppObject * __this /* static, unused */, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AppsFlyer::trackRichEvent(System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern "C"  void AppsFlyer_trackRichEvent_m3152788389 (Il2CppObject * __this /* static, unused */, String_t* p0, Dictionary_2_t3943999495 * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AppsFlyer::setAppID(System.String)
extern "C"  void AppsFlyer_setAppID_m3577374119 (Il2CppObject * __this /* static, unused */, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AppsFlyer::getConversionData()
extern "C"  void AppsFlyer_getConversionData_m2632946641 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AppsFlyer::trackAppLaunch()
extern "C"  void AppsFlyer_trackAppLaunch_m1057024514 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AppsFlyer::setCustomerUserID(System.String)
extern "C"  void AppsFlyer_setCustomerUserID_m1797548495 (Il2CppObject * __this /* static, unused */, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AppsFlyer::setCurrencyCode(System.String)
extern "C"  void AppsFlyer_setCurrencyCode_m2729779557 (Il2CppObject * __this /* static, unused */, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AppsFlyer::setIsSandbox(System.Boolean)
extern "C"  void AppsFlyer_setIsSandbox_m23520209 (Il2CppObject * __this /* static, unused */, bool p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AppsFlyer::validateReceipt(System.String,System.String,System.String,System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern "C"  void AppsFlyer_validateReceipt_m926842288 (Il2CppObject * __this /* static, unused */, String_t* p0, String_t* p1, String_t* p2, String_t* p3, Dictionary_2_t3943999495 * p4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.iOS.NotificationServices::RegisterForNotifications(UnityEngine.iOS.NotificationType)
extern "C"  void NotificationServices_RegisterForNotifications_m2431930612 (Il2CppObject * __this /* static, unused */, int32_t p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.Encoding System.Text.Encoding::get_ASCII()
extern "C"  Encoding_t663144255 * Encoding_get_ASCII_m2727409419 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AppsFlyer::registerUninstall(System.Byte[])
extern "C"  void AppsFlyer_registerUninstall_m664703751 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t3397334013* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Input::GetKeyDown(UnityEngine.KeyCode)
extern "C"  bool Input_GetKeyDown_m1771960377 (Il2CppObject * __this /* static, unused */, int32_t p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] UnityEngine.iOS.NotificationServices::get_deviceToken()
extern "C"  ByteU5BU5D_t3397334013* NotificationServices_get_deviceToken_m552877243 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AF_Sample_BGScript::pressed()
extern "C"  void AF_Sample_BGScript_pressed_m2182290796 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.String,System.String)
extern "C"  String_t* String_Concat_m2596409543 (Il2CppObject * __this /* static, unused */, String_t* p0, String_t* p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.MonoBehaviour::print(System.Object)
extern "C"  void MonoBehaviour_print_m3437620292 (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.String::Contains(System.String)
extern "C"  bool String_Contains_m4017059963 (String_t* __this, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void AF_Sample_BGScript::.ctor()
extern "C"  void AF_Sample_BGScript__ctor_m3934050062 (AF_Sample_BGScript_t3397449209 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AF_Sample_BGScript::Start()
extern "C"  void AF_Sample_BGScript_Start_m1083749446 (AF_Sample_BGScript_t3397449209 * __this, const MethodInfo* method)
{
	{
		// Check.SetActive (false);
		GameObject_t1756533147 * L_0 = __this->get_Check_4();
		// Check.SetActive (false);
		NullCheck(L_0);
		GameObject_SetActive_m2887581199(L_0, (bool)0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void AF_Sample_BGScript::Update()
extern "C"  void AF_Sample_BGScript_Update_m1473604093 (AF_Sample_BGScript_t3397449209 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AF_Sample_BGScript_Update_m1473604093_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (isPressed) {
		IL2CPP_RUNTIME_CLASS_INIT(AF_Sample_BGScript_t3397449209_il2cpp_TypeInfo_var);
		bool L_0 = ((AF_Sample_BGScript_t3397449209_StaticFields*)AF_Sample_BGScript_t3397449209_il2cpp_TypeInfo_var->static_fields)->get_isPressed_6();
		if (!L_0)
		{
			goto IL_0020;
		}
	}
	{
		// StartCoroutine (PressAnim ());
		// StartCoroutine (PressAnim ());
		Il2CppObject * L_1 = AF_Sample_BGScript_PressAnim_m2772021084(__this, /*hidden argument*/NULL);
		// StartCoroutine (PressAnim ());
		MonoBehaviour_StartCoroutine_m2470621050(__this, L_1, /*hidden argument*/NULL);
		// isPressed = false;
		IL2CPP_RUNTIME_CLASS_INIT(AF_Sample_BGScript_t3397449209_il2cpp_TypeInfo_var);
		((AF_Sample_BGScript_t3397449209_StaticFields*)AF_Sample_BGScript_t3397449209_il2cpp_TypeInfo_var->static_fields)->set_isPressed_6((bool)0);
	}

IL_0020:
	{
		// }
		return;
	}
}
// System.Void AF_Sample_BGScript::pressed()
extern "C"  void AF_Sample_BGScript_pressed_m2182290796 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AF_Sample_BGScript_pressed_m2182290796_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// isPressed = true;
		IL2CPP_RUNTIME_CLASS_INIT(AF_Sample_BGScript_t3397449209_il2cpp_TypeInfo_var);
		((AF_Sample_BGScript_t3397449209_StaticFields*)AF_Sample_BGScript_t3397449209_il2cpp_TypeInfo_var->static_fields)->set_isPressed_6((bool)1);
		// }
		return;
	}
}
// System.Collections.IEnumerator AF_Sample_BGScript::PressAnim()
extern "C"  Il2CppObject * AF_Sample_BGScript_PressAnim_m2772021084 (AF_Sample_BGScript_t3397449209 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AF_Sample_BGScript_PressAnim_m2772021084_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CPressAnimU3Ec__Iterator0_t3813865769 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	{
		U3CPressAnimU3Ec__Iterator0_t3813865769 * L_0 = (U3CPressAnimU3Ec__Iterator0_t3813865769 *)il2cpp_codegen_object_new(U3CPressAnimU3Ec__Iterator0_t3813865769_il2cpp_TypeInfo_var);
		U3CPressAnimU3Ec__Iterator0__ctor_m727930690(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CPressAnimU3Ec__Iterator0_t3813865769 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U24this_0(__this);
		U3CPressAnimU3Ec__Iterator0_t3813865769 * L_2 = V_0;
		V_1 = L_2;
		goto IL_0014;
	}

IL_0014:
	{
		Il2CppObject * L_3 = V_1;
		return L_3;
	}
}
// System.Void AF_Sample_BGScript::.cctor()
extern "C"  void AF_Sample_BGScript__cctor_m3276419787 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AF_Sample_BGScript__cctor_m3276419787_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// private static bool isPressed=false;
		((AF_Sample_BGScript_t3397449209_StaticFields*)AF_Sample_BGScript_t3397449209_il2cpp_TypeInfo_var->static_fields)->set_isPressed_6((bool)0);
		return;
	}
}
// System.Void AF_Sample_BGScript/<PressAnim>c__Iterator0::.ctor()
extern "C"  void U3CPressAnimU3Ec__Iterator0__ctor_m727930690 (U3CPressAnimU3Ec__Iterator0_t3813865769 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean AF_Sample_BGScript/<PressAnim>c__Iterator0::MoveNext()
extern "C"  bool U3CPressAnimU3Ec__Iterator0_MoveNext_m1503219590 (U3CPressAnimU3Ec__Iterator0_t3813865769 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CPressAnimU3Ec__Iterator0_MoveNext_m1503219590_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_3();
		V_0 = L_0;
		__this->set_U24PC_3((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0021;
			}
			case 1:
			{
				goto IL_0082;
			}
		}
	}
	{
		goto IL_00c5;
	}

IL_0021:
	{
		// Check.SetActive (true);
		AF_Sample_BGScript_t3397449209 * L_2 = __this->get_U24this_0();
		NullCheck(L_2);
		GameObject_t1756533147 * L_3 = L_2->get_Check_4();
		// Check.SetActive (true);
		NullCheck(L_3);
		GameObject_SetActive_m2887581199(L_3, (bool)1, /*hidden argument*/NULL);
		// ButtonText.GetComponent<Text> ().text = "Event Tracked!";
		AF_Sample_BGScript_t3397449209 * L_4 = __this->get_U24this_0();
		NullCheck(L_4);
		GameObject_t1756533147 * L_5 = L_4->get_ButtonText_3();
		// ButtonText.GetComponent<Text> ().text = "Event Tracked!";
		NullCheck(L_5);
		Text_t356221433 * L_6 = GameObject_GetComponent_TisText_t356221433_m1217399699(L_5, /*hidden argument*/GameObject_GetComponent_TisText_t356221433_m1217399699_MethodInfo_var);
		// ButtonText.GetComponent<Text> ().text = "Event Tracked!";
		NullCheck(L_6);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_6, _stringLiteral1548508307);
		// Button.SetActive (false);
		AF_Sample_BGScript_t3397449209 * L_7 = __this->get_U24this_0();
		NullCheck(L_7);
		GameObject_t1756533147 * L_8 = L_7->get_Button_2();
		// Button.SetActive (false);
		NullCheck(L_8);
		GameObject_SetActive_m2887581199(L_8, (bool)0, /*hidden argument*/NULL);
		// yield return new WaitForSeconds(3.5f);
		// yield return new WaitForSeconds(3.5f);
		WaitForSeconds_t3839502067 * L_9 = (WaitForSeconds_t3839502067 *)il2cpp_codegen_object_new(WaitForSeconds_t3839502067_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m1990515539(L_9, (3.5f), /*hidden argument*/NULL);
		__this->set_U24current_1(L_9);
		bool L_10 = __this->get_U24disposing_2();
		if (L_10)
		{
			goto IL_007d;
		}
	}
	{
		__this->set_U24PC_3(1);
	}

IL_007d:
	{
		goto IL_00c7;
	}

IL_0082:
	{
		// Check.SetActive (false);
		AF_Sample_BGScript_t3397449209 * L_11 = __this->get_U24this_0();
		NullCheck(L_11);
		GameObject_t1756533147 * L_12 = L_11->get_Check_4();
		// Check.SetActive (false);
		NullCheck(L_12);
		GameObject_SetActive_m2887581199(L_12, (bool)0, /*hidden argument*/NULL);
		// ButtonText.GetComponent<Text> ().text = "Track Event";
		AF_Sample_BGScript_t3397449209 * L_13 = __this->get_U24this_0();
		NullCheck(L_13);
		GameObject_t1756533147 * L_14 = L_13->get_ButtonText_3();
		// ButtonText.GetComponent<Text> ().text = "Track Event";
		NullCheck(L_14);
		Text_t356221433 * L_15 = GameObject_GetComponent_TisText_t356221433_m1217399699(L_14, /*hidden argument*/GameObject_GetComponent_TisText_t356221433_m1217399699_MethodInfo_var);
		// ButtonText.GetComponent<Text> ().text = "Track Event";
		NullCheck(L_15);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_15, _stringLiteral3229045867);
		// Button.SetActive (true);
		AF_Sample_BGScript_t3397449209 * L_16 = __this->get_U24this_0();
		NullCheck(L_16);
		GameObject_t1756533147 * L_17 = L_16->get_Button_2();
		// Button.SetActive (true);
		NullCheck(L_17);
		GameObject_SetActive_m2887581199(L_17, (bool)1, /*hidden argument*/NULL);
		// }
		__this->set_U24PC_3((-1));
	}

IL_00c5:
	{
		return (bool)0;
	}

IL_00c7:
	{
		return (bool)1;
	}
}
// System.Object AF_Sample_BGScript/<PressAnim>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CPressAnimU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1182631106 (U3CPressAnimU3Ec__Iterator0_t3813865769 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		Il2CppObject * L_1 = V_0;
		return L_1;
	}
}
// System.Object AF_Sample_BGScript/<PressAnim>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CPressAnimU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2185484154 (U3CPressAnimU3Ec__Iterator0_t3813865769 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		Il2CppObject * L_1 = V_0;
		return L_1;
	}
}
// System.Void AF_Sample_BGScript/<PressAnim>c__Iterator0::Dispose()
extern "C"  void U3CPressAnimU3Ec__Iterator0_Dispose_m784815307 (U3CPressAnimU3Ec__Iterator0_t3813865769 * __this, const MethodInfo* method)
{
	{
		__this->set_U24disposing_2((bool)1);
		__this->set_U24PC_3((-1));
		return;
	}
}
// System.Void AF_Sample_BGScript/<PressAnim>c__Iterator0::Reset()
extern "C"  void U3CPressAnimU3Ec__Iterator0_Reset_m2688908769 (U3CPressAnimU3Ec__Iterator0_t3813865769 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CPressAnimU3Ec__Iterator0_Reset_m2688908769_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void StartUp::.ctor()
extern "C"  void StartUp__ctor_m2886309856 (StartUp_t1662321771 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void StartUp::Start()
extern "C"  void StartUp_Start_m1623301020 (StartUp_t1662321771 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StartUp_Start_m1623301020_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	Dictionary_2_t3943999495 * V_1 = NULL;
	Dictionary_2_t3943999495 * V_2 = NULL;
	{
		// Application.runInBackground = true;
		// Application.runInBackground = true;
		Application_set_runInBackground_m3543179741(NULL /*static, unused*/, (bool)1, /*hidden argument*/NULL);
		// Screen.orientation = ScreenOrientation.Portrait;
		// Screen.orientation = ScreenOrientation.Portrait;
		Screen_set_orientation_m413080199(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		// DontDestroyOnLoad (this);
		// DontDestroyOnLoad (this);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_DontDestroyOnLoad_m2330762974(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		// string unityEventName = "test";
		V_0 = _stringLiteral3423761286;
		// Dictionary<string, string> unityEventValues = new Dictionary<string, string> ();
		Dictionary_2_t3943999495 * L_0 = (Dictionary_2_t3943999495 *)il2cpp_codegen_object_new(Dictionary_2_t3943999495_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m28427054(L_0, /*hidden argument*/Dictionary_2__ctor_m28427054_MethodInfo_var);
		V_1 = L_0;
		// unityEventValues.Add ("testValues1", "testValues2");
		Dictionary_2_t3943999495 * L_1 = V_1;
		// unityEventValues.Add ("testValues1", "testValues2");
		NullCheck(L_1);
		Dictionary_2_Add_m3686231158(L_1, _stringLiteral2801716075, _stringLiteral2801716078, /*hidden argument*/Dictionary_2_Add_m3686231158_MethodInfo_var);
		// AppsFlyer.setAppsFlyerKey ("iErwCWL8p5wapsDjnDBQQb");
		// AppsFlyer.setAppsFlyerKey ("iErwCWL8p5wapsDjnDBQQb");
		AppsFlyer_setAppsFlyerKey_m1791358164(NULL /*static, unused*/, _stringLiteral3259574281, /*hidden argument*/NULL);
		// AppsFlyer.trackRichEvent(unityEventName, unityEventValues);
		String_t* L_2 = V_0;
		Dictionary_2_t3943999495 * L_3 = V_1;
		// AppsFlyer.trackRichEvent(unityEventName, unityEventValues);
		AppsFlyer_trackRichEvent_m3152788389(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		// AppsFlyer.setAppID ("300200112");
		// AppsFlyer.setAppID ("300200112");
		AppsFlyer_setAppID_m3577374119(NULL /*static, unused*/, _stringLiteral1348778707, /*hidden argument*/NULL);
		// AppsFlyer.getConversionData ();
		AppsFlyer_getConversionData_m2632946641(NULL /*static, unused*/, /*hidden argument*/NULL);
		// AppsFlyer.trackAppLaunch ();
		AppsFlyer_trackAppLaunch_m1057024514(NULL /*static, unused*/, /*hidden argument*/NULL);
		// AppsFlyer.setCustomerUserID ("Unity Test customerUserID");
		// AppsFlyer.setCustomerUserID ("Unity Test customerUserID");
		AppsFlyer_setCustomerUserID_m1797548495(NULL /*static, unused*/, _stringLiteral337074973, /*hidden argument*/NULL);
		// AppsFlyer.setCurrencyCode ("EUR");
		// AppsFlyer.setCurrencyCode ("EUR");
		AppsFlyer_setCurrencyCode_m2729779557(NULL /*static, unused*/, _stringLiteral884246882, /*hidden argument*/NULL);
		// AppsFlyer.setIsSandbox(true);
		// AppsFlyer.setIsSandbox(true);
		AppsFlyer_setIsSandbox_m23520209(NULL /*static, unused*/, (bool)1, /*hidden argument*/NULL);
		// Dictionary<string, string> validateTest = new Dictionary<string, string>();
		Dictionary_2_t3943999495 * L_4 = (Dictionary_2_t3943999495 *)il2cpp_codegen_object_new(Dictionary_2_t3943999495_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m28427054(L_4, /*hidden argument*/Dictionary_2__ctor_m28427054_MethodInfo_var);
		V_2 = L_4;
		// AppsFlyer.validateReceipt( "GSNA_CoinsPack1", "10", "USD", "410000253818916", validateTest );
		Dictionary_2_t3943999495 * L_5 = V_2;
		// AppsFlyer.validateReceipt( "GSNA_CoinsPack1", "10", "USD", "410000253818916", validateTest );
		AppsFlyer_validateReceipt_m926842288(NULL /*static, unused*/, _stringLiteral1881721028, _stringLiteral98844765, _stringLiteral1690816450, _stringLiteral3035521982, L_5, /*hidden argument*/NULL);
		// UnityEngine.iOS.NotificationServices.RegisterForNotifications (UnityEngine.iOS.NotificationType.Alert | UnityEngine.iOS.NotificationType.Badge | UnityEngine.iOS.NotificationType.Sound);
		// UnityEngine.iOS.NotificationServices.RegisterForNotifications (UnityEngine.iOS.NotificationType.Alert | UnityEngine.iOS.NotificationType.Badge | UnityEngine.iOS.NotificationType.Sound);
		NotificationServices_RegisterForNotifications_m2431930612(NULL /*static, unused*/, 7, /*hidden argument*/NULL);
		// Screen.orientation = ScreenOrientation.Portrait;
		// Screen.orientation = ScreenOrientation.Portrait;
		Screen_set_orientation_m413080199(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		// AppsFlyer.registerUninstall (Encoding.ASCII.GetBytes("72b0ab229608d0e0e500c1fd7df4e969303fbefbc230b45509b0c90109be49f7"));
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t663144255_il2cpp_TypeInfo_var);
		Encoding_t663144255 * L_6 = Encoding_get_ASCII_m2727409419(NULL /*static, unused*/, /*hidden argument*/NULL);
		// AppsFlyer.registerUninstall (Encoding.ASCII.GetBytes("72b0ab229608d0e0e500c1fd7df4e969303fbefbc230b45509b0c90109be49f7"));
		NullCheck(L_6);
		ByteU5BU5D_t3397334013* L_7 = VirtFuncInvoker1< ByteU5BU5D_t3397334013*, String_t* >::Invoke(10 /* System.Byte[] System.Text.Encoding::GetBytes(System.String) */, L_6, _stringLiteral621716435);
		// AppsFlyer.registerUninstall (Encoding.ASCII.GetBytes("72b0ab229608d0e0e500c1fd7df4e969303fbefbc230b45509b0c90109be49f7"));
		AppsFlyer_registerUninstall_m664703751(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		// AppsFlyer.trackRichEvent(unityEventName, unityEventValues);
		String_t* L_8 = V_0;
		Dictionary_2_t3943999495 * L_9 = V_1;
		// AppsFlyer.trackRichEvent(unityEventName, unityEventValues);
		AppsFlyer_trackRichEvent_m3152788389(NULL /*static, unused*/, L_8, L_9, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void StartUp::Update()
extern "C"  void StartUp_Update_m1673313907 (StartUp_t1662321771 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StartUp_Update_m1673313907_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ByteU5BU5D_t3397334013* V_0 = NULL;
	{
		// if (Input.GetKeyDown (KeyCode.Escape)) {
		// if (Input.GetKeyDown (KeyCode.Escape)) {
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_0 = Input_GetKeyDown_m1771960377(NULL /*static, unused*/, ((int32_t)27), /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_000f;
		}
	}
	{
	}

IL_000f:
	{
		// if (!tokenSent) {
		bool L_1 = __this->get_tokenSent_3();
		if (L_1)
		{
			goto IL_0037;
		}
	}
	{
		// byte[] token = UnityEngine.iOS.NotificationServices.deviceToken;
		ByteU5BU5D_t3397334013* L_2 = NotificationServices_get_deviceToken_m552877243(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_2;
		// if (token != null) {
		ByteU5BU5D_t3397334013* L_3 = V_0;
		if (!L_3)
		{
			goto IL_0036;
		}
	}
	{
		// AppsFlyer.registerUninstall (token);
		ByteU5BU5D_t3397334013* L_4 = V_0;
		// AppsFlyer.registerUninstall (token);
		AppsFlyer_registerUninstall_m664703751(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		// tokenSent = true;
		__this->set_tokenSent_3((bool)1);
	}

IL_0036:
	{
	}

IL_0037:
	{
		// }
		return;
	}
}
// System.Void StartUp::Purchase()
extern "C"  void StartUp_Purchase_m4261947645 (StartUp_t1662321771 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StartUp_Purchase_m4261947645_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Dictionary_2_t3943999495 * V_0 = NULL;
	{
		// Dictionary<string, string> eventValue = new Dictionary<string,string> ();
		Dictionary_2_t3943999495 * L_0 = (Dictionary_2_t3943999495 *)il2cpp_codegen_object_new(Dictionary_2_t3943999495_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m28427054(L_0, /*hidden argument*/Dictionary_2__ctor_m28427054_MethodInfo_var);
		V_0 = L_0;
		// eventValue.Add("af_revenue","200");
		Dictionary_2_t3943999495 * L_1 = V_0;
		// eventValue.Add("af_revenue","200");
		NullCheck(L_1);
		Dictionary_2_Add_m3686231158(L_1, _stringLiteral2052050410, _stringLiteral104526608, /*hidden argument*/Dictionary_2_Add_m3686231158_MethodInfo_var);
		// eventValue.Add("af_content_type","category_a");
		Dictionary_2_t3943999495 * L_2 = V_0;
		// eventValue.Add("af_content_type","category_a");
		NullCheck(L_2);
		Dictionary_2_Add_m3686231158(L_2, _stringLiteral3232720084, _stringLiteral3941577958, /*hidden argument*/Dictionary_2_Add_m3686231158_MethodInfo_var);
		// eventValue.Add("af_content_id","1234567");
		Dictionary_2_t3943999495 * L_3 = V_0;
		// eventValue.Add("af_content_id","1234567");
		NullCheck(L_3);
		Dictionary_2_Add_m3686231158(L_3, _stringLiteral1598873091, _stringLiteral3686229262, /*hidden argument*/Dictionary_2_Add_m3686231158_MethodInfo_var);
		// AppsFlyer.trackRichEvent("af_purchase", eventValue);
		Dictionary_2_t3943999495 * L_4 = V_0;
		// AppsFlyer.trackRichEvent("af_purchase", eventValue);
		AppsFlyer_trackRichEvent_m3152788389(NULL /*static, unused*/, _stringLiteral3344737995, L_4, /*hidden argument*/NULL);
		// AF_Sample_BGScript.pressed ();
		IL2CPP_RUNTIME_CLASS_INIT(AF_Sample_BGScript_t3397449209_il2cpp_TypeInfo_var);
		AF_Sample_BGScript_pressed_m2182290796(NULL /*static, unused*/, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void StartUp::didReceiveConversionData(System.String)
extern "C"  void StartUp_didReceiveConversionData_m3983156178 (StartUp_t1662321771 * __this, String_t* ___conversionData0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StartUp_didReceiveConversionData_m3983156178_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// print ("AppsFlyerTrackerCallbacks:: got conversion data = " + conversionData);
		String_t* L_0 = ___conversionData0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral1259047537, L_0, /*hidden argument*/NULL);
		// print ("AppsFlyerTrackerCallbacks:: got conversion data = " + conversionData);
		MonoBehaviour_print_m3437620292(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		// if (conversionData.Contains ("Non")) {
		String_t* L_2 = ___conversionData0;
		// if (conversionData.Contains ("Non")) {
		NullCheck(L_2);
		bool L_3 = String_Contains_m4017059963(L_2, _stringLiteral1502599639, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_003d;
		}
	}
	{
		// text.GetComponent<Text> ().text = "Non-Organic Install";
		GameObject_t1756533147 * L_4 = __this->get_text_2();
		// text.GetComponent<Text> ().text = "Non-Organic Install";
		NullCheck(L_4);
		Text_t356221433 * L_5 = GameObject_GetComponent_TisText_t356221433_m1217399699(L_4, /*hidden argument*/GameObject_GetComponent_TisText_t356221433_m1217399699_MethodInfo_var);
		// text.GetComponent<Text> ().text = "Non-Organic Install";
		NullCheck(L_5);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_5, _stringLiteral607539246);
		goto IL_0054;
	}

IL_003d:
	{
		// text.GetComponent<Text> ().text = "Organic Install";
		GameObject_t1756533147 * L_6 = __this->get_text_2();
		// text.GetComponent<Text> ().text = "Organic Install";
		NullCheck(L_6);
		Text_t356221433 * L_7 = GameObject_GetComponent_TisText_t356221433_m1217399699(L_6, /*hidden argument*/GameObject_GetComponent_TisText_t356221433_m1217399699_MethodInfo_var);
		// text.GetComponent<Text> ().text = "Organic Install";
		NullCheck(L_7);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_7, _stringLiteral1824391588);
	}

IL_0054:
	{
		// }
		return;
	}
}
// System.Void StartUp::didReceiveConversionDataWithError(System.String)
extern "C"  void StartUp_didReceiveConversionDataWithError_m2120681874 (StartUp_t1662321771 * __this, String_t* ___error0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StartUp_didReceiveConversionDataWithError_m2120681874_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// print ("AppsFlyerTrackerCallbacks:: got conversion data error = " + error);
		String_t* L_0 = ___error0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral1294667269, L_0, /*hidden argument*/NULL);
		// print ("AppsFlyerTrackerCallbacks:: got conversion data error = " + error);
		MonoBehaviour_print_m3437620292(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void StartUp::onAppOpenAttribution(System.String)
extern "C"  void StartUp_onAppOpenAttribution_m1833310285 (StartUp_t1662321771 * __this, String_t* ___validateResult0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StartUp_onAppOpenAttribution_m1833310285_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// print ("AppsFlyerTrackerCallbacks:: got onAppOpenAttribution  = " + validateResult);
		String_t* L_0 = ___validateResult0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral184702546, L_0, /*hidden argument*/NULL);
		// print ("AppsFlyerTrackerCallbacks:: got onAppOpenAttribution  = " + validateResult);
		MonoBehaviour_print_m3437620292(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void StartUp::onAppOpenAttributionFailure(System.String)
extern "C"  void StartUp_onAppOpenAttributionFailure_m2441954989 (StartUp_t1662321771 * __this, String_t* ___error0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StartUp_onAppOpenAttributionFailure_m2441954989_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// print ("AppsFlyerTrackerCallbacks:: got onAppOpenAttributionFailure error = " + error);
		String_t* L_0 = ___error0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral3688921710, L_0, /*hidden argument*/NULL);
		// print ("AppsFlyerTrackerCallbacks:: got onAppOpenAttributionFailure error = " + error);
		MonoBehaviour_print_m3437620292(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
