﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_AF_Sample_BGScript_U3CPressAnimU3813865769.h"
#include "AssemblyU2DCSharp_StartUp1662321771.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1700 = { sizeof (U3CPressAnimU3Ec__Iterator0_t3813865769), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1700[4] = 
{
	U3CPressAnimU3Ec__Iterator0_t3813865769::get_offset_of_U24this_0(),
	U3CPressAnimU3Ec__Iterator0_t3813865769::get_offset_of_U24current_1(),
	U3CPressAnimU3Ec__Iterator0_t3813865769::get_offset_of_U24disposing_2(),
	U3CPressAnimU3Ec__Iterator0_t3813865769::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1701 = { sizeof (StartUp_t1662321771), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1701[2] = 
{
	StartUp_t1662321771::get_offset_of_text_2(),
	StartUp_t1662321771::get_offset_of_tokenSent_3(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
