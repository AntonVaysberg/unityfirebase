﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharpU2Dfirstpass_U3CModuleU3E3783534214.h"
#include "AssemblyU2DCSharpU2Dfirstpass_AFInAppEvents610589710.h"
#include "mscorlib_System_Void1841601450.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharpU2Dfirstpass_AppsFlyer2800548462.h"
#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "mscorlib_System_Byte3683104436.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g3943999495.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En969056901.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3601534125.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21701344717.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_g38854645.h"
#include "AssemblyU2DCSharpU2Dfirstpass_AppsFlyerTrackerCall3562121710.h"
#include "UnityEngine_UI_UnityEngine_UI_Text356221433.h"

// AFInAppEvents
struct AFInAppEvents_t610589710;
// System.Object
struct Il2CppObject;
// AppsFlyer
struct AppsFlyer_t2800548462;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t1158329972;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t3943999495;
// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct Dictionary_2_t2281509423;
// System.String[]
struct StringU5BU5D_t1642385972;
// AppsFlyerTrackerCallbacks
struct AppsFlyerTrackerCallbacks_t3562121710;
extern Il2CppCodeGenString* _stringLiteral3457038533;
extern const uint32_t AppsFlyer_trackEvent_m3353671724_MetadataUsageId;
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_GetEnumerator_m2456297074_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m4048655796_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Key_m2641560888_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Value_m2909389725_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m52089307_MethodInfo_var;
extern const MethodInfo* Enumerator_Dispose_m882561911_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral371857150;
extern Il2CppCodeGenString* _stringLiteral372029329;
extern Il2CppCodeGenString* _stringLiteral372029352;
extern const uint32_t AppsFlyer_trackRichEvent_m3152788389_MetadataUsageId;
extern const uint32_t AppsFlyer_validateReceipt_m926842288_MetadataUsageId;
extern const uint32_t AppsFlyer_handlePushNotification_m3382247817_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2850019861;
extern const uint32_t AppsFlyerTrackerCallbacks_Start_m2082414881_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1259047537;
extern const uint32_t AppsFlyerTrackerCallbacks_didReceiveConversionData_m3717905989_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1294667269;
extern const uint32_t AppsFlyerTrackerCallbacks_didReceiveConversionDataWithError_m3101201123_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral4209498249;
extern const uint32_t AppsFlyerTrackerCallbacks_didFinishValidateReceipt_m1223936711_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1909124487;
extern const uint32_t AppsFlyerTrackerCallbacks_didFinishValidateReceiptWithError_m803271077_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral184702546;
extern const uint32_t AppsFlyerTrackerCallbacks_onAppOpenAttribution_m4211417896_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral3688921710;
extern const uint32_t AppsFlyerTrackerCallbacks_onAppOpenAttributionFailure_m1384204616_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral4055123575;
extern const uint32_t AppsFlyerTrackerCallbacks_onInAppBillingSuccess_m2018164444_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral3776751665;
extern const uint32_t AppsFlyerTrackerCallbacks_onInAppBillingFailure_m123106739_MetadataUsageId;
extern const uint32_t AppsFlyerTrackerCallbacks_printCallback_m2540129811_MetadataUsageId;

// System.Byte[]
struct ByteU5BU5D_t3397334013  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) uint8_t m_Items[1];

public:
	inline uint8_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline uint8_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, uint8_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline uint8_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline uint8_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, uint8_t value)
	{
		m_Items[index] = value;
	}
};
// System.String[]
struct StringU5BU5D_t1642385972  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) String_t* m_Items[1];

public:
	inline String_t* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline String_t** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, String_t* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline String_t* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline String_t** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, String_t* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};


// System.Collections.Generic.Dictionary`2/Enumerator<!0,!1> System.Collections.Generic.Dictionary`2<System.Object,System.Object>::GetEnumerator()
extern "C"  Enumerator_t3601534125  Dictionary_2_GetEnumerator_m3077639147_gshared (Dictionary_2_t2281509423 * __this, const MethodInfo* method);
// System.Collections.Generic.KeyValuePair`2<!0,!1> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::get_Current()
extern "C"  KeyValuePair_2_t38854645  Enumerator_get_Current_m1091361971_gshared (Enumerator_t3601534125 * __this, const MethodInfo* method);
// !0 System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::get_Key()
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m3385717033_gshared (KeyValuePair_2_t38854645 * __this, const MethodInfo* method);
// !1 System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::get_Value()
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m1251901674_gshared (KeyValuePair_2_t38854645 * __this, const MethodInfo* method);
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3349738440_gshared (Enumerator_t3601534125 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m1905011127_gshared (Enumerator_t3601534125 * __this, const MethodInfo* method);

// System.Void System.Object::.ctor()
extern "C"  void Object__ctor_m2551263788 (Il2CppObject * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.MonoBehaviour::.ctor()
extern "C"  void MonoBehaviour__ctor_m2464341955 (MonoBehaviour_t1158329972 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AppsFlyer::mTrackEvent(System.String,System.String)
extern "C"  void AppsFlyer_mTrackEvent_m2869132329 (Il2CppObject * __this /* static, unused */, String_t* ___eventName0, String_t* ___eventValue1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.MonoBehaviour::print(System.Object)
extern "C"  void MonoBehaviour_print_m3437620292 (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AppsFlyer::mSetCurrencyCode(System.String)
extern "C"  void AppsFlyer_mSetCurrencyCode_m2952676942 (Il2CppObject * __this /* static, unused */, String_t* ___currencyCode0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AppsFlyer::mSetCustomerUserID(System.String)
extern "C"  void AppsFlyer_mSetCustomerUserID_m4292130114 (Il2CppObject * __this /* static, unused */, String_t* ___customerUserID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AppsFlyer::mSetAppsFlyerDevKey(System.String)
extern "C"  void AppsFlyer_mSetAppsFlyerDevKey_m3489814122 (Il2CppObject * __this /* static, unused */, String_t* ___devKey0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AppsFlyer::mTrackAppLaunch()
extern "C"  void AppsFlyer_mTrackAppLaunch_m1688197437 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AppsFlyer::mSetAppID(System.String)
extern "C"  void AppsFlyer_mSetAppID_m3092783132 (Il2CppObject * __this /* static, unused */, String_t* ___appleAppId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2/Enumerator<!0,!1> System.Collections.Generic.Dictionary`2<System.String,System.String>::GetEnumerator()
#define Dictionary_2_GetEnumerator_m2456297074(__this, method) ((  Enumerator_t969056901  (*) (Dictionary_2_t3943999495 *, const MethodInfo*))Dictionary_2_GetEnumerator_m3077639147_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<!0,!1> System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.String>::get_Current()
#define Enumerator_get_Current_m4048655796(__this, method) ((  KeyValuePair_2_t1701344717  (*) (Enumerator_t969056901 *, const MethodInfo*))Enumerator_get_Current_m1091361971_gshared)(__this, method)
// !0 System.Collections.Generic.KeyValuePair`2<System.String,System.String>::get_Key()
#define KeyValuePair_2_get_Key_m2641560888(__this, method) ((  String_t* (*) (KeyValuePair_2_t1701344717 *, const MethodInfo*))KeyValuePair_2_get_Key_m3385717033_gshared)(__this, method)
// !1 System.Collections.Generic.KeyValuePair`2<System.String,System.String>::get_Value()
#define KeyValuePair_2_get_Value_m2909389725(__this, method) ((  String_t* (*) (KeyValuePair_2_t1701344717 *, const MethodInfo*))KeyValuePair_2_get_Value_m1251901674_gshared)(__this, method)
// System.String System.String::Concat(System.String[])
extern "C"  String_t* String_Concat_m626692867 (Il2CppObject * __this /* static, unused */, StringU5BU5D_t1642385972* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.String>::MoveNext()
#define Enumerator_MoveNext_m52089307(__this, method) ((  bool (*) (Enumerator_t969056901 *, const MethodInfo*))Enumerator_MoveNext_m3349738440_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.String>::Dispose()
#define Enumerator_Dispose_m882561911(__this, method) ((  void (*) (Enumerator_t969056901 *, const MethodInfo*))Enumerator_Dispose_m1905011127_gshared)(__this, method)
// System.Void AppsFlyer::mTrackRichEvent(System.String,System.String)
extern "C"  void AppsFlyer_mTrackRichEvent_m3339608949 (Il2CppObject * __this /* static, unused */, String_t* ___eventName0, String_t* ___eventValues1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AppsFlyer::mValidateReceipt(System.String,System.String,System.String,System.String,System.String)
extern "C"  void AppsFlyer_mValidateReceipt_m2008272410 (Il2CppObject * __this /* static, unused */, String_t* ___productIdentifier0, String_t* ___price1, String_t* ___currency2, String_t* ___transactionId3, String_t* ___additionalParams4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AppsFlyer::mSetIsDebug(System.Boolean)
extern "C"  void AppsFlyer_mSetIsDebug_m3555622242 (Il2CppObject * __this /* static, unused */, bool ___isDebug0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AppsFlyer::mSetIsSandbox(System.Boolean)
extern "C"  void AppsFlyer_mSetIsSandbox_m2595239082 (Il2CppObject * __this /* static, unused */, bool ___isSandbox0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AppsFlyer::mGetConversionData()
extern "C"  void AppsFlyer_mGetConversionData_m181512626 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String AppsFlyer::mGetAppsFlyerId()
extern "C"  String_t* AppsFlyer_mGetAppsFlyerId_m3223677698 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AppsFlyer::mHandleOpenUrl(System.String,System.String,System.String)
extern "C"  void AppsFlyer_mHandleOpenUrl_m456505413 (Il2CppObject * __this /* static, unused */, String_t* ___url0, String_t* ___sourceApplication1, String_t* ___annotation2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AppsFlyer::mHandlePushNotification(System.String)
extern "C"  void AppsFlyer_mHandlePushNotification_m1207167649 (Il2CppObject * __this /* static, unused */, String_t* ___payload0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AppsFlyer::mRegisterUninstall(System.Byte[])
extern "C"  void AppsFlyer_mRegisterUninstall_m538787490 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___pushToken0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.String,System.String)
extern "C"  String_t* String_Concat_m2596409543 (Il2CppObject * __this /* static, unused */, String_t* p0, String_t* p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AppsFlyerTrackerCallbacks::printCallback(System.String)
extern "C"  void AppsFlyerTrackerCallbacks_printCallback_m2540129811 (AppsFlyerTrackerCallbacks_t3562121710 * __this, String_t* ___str0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.String,System.String,System.String)
extern "C"  String_t* String_Concat_m612901809 (Il2CppObject * __this /* static, unused */, String_t* p0, String_t* p1, String_t* p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void AFInAppEvents::.ctor()
extern "C"  void AFInAppEvents__ctor_m635870687 (AFInAppEvents_t610589710 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AppsFlyer::.ctor()
extern "C"  void AppsFlyer__ctor_m4010506095 (AppsFlyer_t2800548462 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
extern "C" void DEFAULT_CALL mTrackEvent(char*, char*);
// System.Void AppsFlyer::mTrackEvent(System.String,System.String)
extern "C"  void AppsFlyer_mTrackEvent_m2869132329 (Il2CppObject * __this /* static, unused */, String_t* ___eventName0, String_t* ___eventValue1, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*, char*);

	// Marshaling of parameter '___eventName0' to native representation
	char* ____eventName0_marshaled = NULL;
	____eventName0_marshaled = il2cpp_codegen_marshal_string(___eventName0);

	// Marshaling of parameter '___eventValue1' to native representation
	char* ____eventValue1_marshaled = NULL;
	____eventValue1_marshaled = il2cpp_codegen_marshal_string(___eventValue1);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(mTrackEvent)(____eventName0_marshaled, ____eventValue1_marshaled);

	// Marshaling cleanup of parameter '___eventName0' native representation
	il2cpp_codegen_marshal_free(____eventName0_marshaled);
	____eventName0_marshaled = NULL;

	// Marshaling cleanup of parameter '___eventValue1' native representation
	il2cpp_codegen_marshal_free(____eventValue1_marshaled);
	____eventValue1_marshaled = NULL;

}
extern "C" void DEFAULT_CALL mSetCurrencyCode(char*);
// System.Void AppsFlyer::mSetCurrencyCode(System.String)
extern "C"  void AppsFlyer_mSetCurrencyCode_m2952676942 (Il2CppObject * __this /* static, unused */, String_t* ___currencyCode0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*);

	// Marshaling of parameter '___currencyCode0' to native representation
	char* ____currencyCode0_marshaled = NULL;
	____currencyCode0_marshaled = il2cpp_codegen_marshal_string(___currencyCode0);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(mSetCurrencyCode)(____currencyCode0_marshaled);

	// Marshaling cleanup of parameter '___currencyCode0' native representation
	il2cpp_codegen_marshal_free(____currencyCode0_marshaled);
	____currencyCode0_marshaled = NULL;

}
extern "C" void DEFAULT_CALL mSetCustomerUserID(char*);
// System.Void AppsFlyer::mSetCustomerUserID(System.String)
extern "C"  void AppsFlyer_mSetCustomerUserID_m4292130114 (Il2CppObject * __this /* static, unused */, String_t* ___customerUserID0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*);

	// Marshaling of parameter '___customerUserID0' to native representation
	char* ____customerUserID0_marshaled = NULL;
	____customerUserID0_marshaled = il2cpp_codegen_marshal_string(___customerUserID0);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(mSetCustomerUserID)(____customerUserID0_marshaled);

	// Marshaling cleanup of parameter '___customerUserID0' native representation
	il2cpp_codegen_marshal_free(____customerUserID0_marshaled);
	____customerUserID0_marshaled = NULL;

}
extern "C" void DEFAULT_CALL mSetAppsFlyerDevKey(char*);
// System.Void AppsFlyer::mSetAppsFlyerDevKey(System.String)
extern "C"  void AppsFlyer_mSetAppsFlyerDevKey_m3489814122 (Il2CppObject * __this /* static, unused */, String_t* ___devKey0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*);

	// Marshaling of parameter '___devKey0' to native representation
	char* ____devKey0_marshaled = NULL;
	____devKey0_marshaled = il2cpp_codegen_marshal_string(___devKey0);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(mSetAppsFlyerDevKey)(____devKey0_marshaled);

	// Marshaling cleanup of parameter '___devKey0' native representation
	il2cpp_codegen_marshal_free(____devKey0_marshaled);
	____devKey0_marshaled = NULL;

}
extern "C" void DEFAULT_CALL mTrackAppLaunch();
// System.Void AppsFlyer::mTrackAppLaunch()
extern "C"  void AppsFlyer_mTrackAppLaunch_m1688197437 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(mTrackAppLaunch)();

}
extern "C" void DEFAULT_CALL mSetAppID(char*);
// System.Void AppsFlyer::mSetAppID(System.String)
extern "C"  void AppsFlyer_mSetAppID_m3092783132 (Il2CppObject * __this /* static, unused */, String_t* ___appleAppId0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*);

	// Marshaling of parameter '___appleAppId0' to native representation
	char* ____appleAppId0_marshaled = NULL;
	____appleAppId0_marshaled = il2cpp_codegen_marshal_string(___appleAppId0);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(mSetAppID)(____appleAppId0_marshaled);

	// Marshaling cleanup of parameter '___appleAppId0' native representation
	il2cpp_codegen_marshal_free(____appleAppId0_marshaled);
	____appleAppId0_marshaled = NULL;

}
extern "C" void DEFAULT_CALL mTrackRichEvent(char*, char*);
// System.Void AppsFlyer::mTrackRichEvent(System.String,System.String)
extern "C"  void AppsFlyer_mTrackRichEvent_m3339608949 (Il2CppObject * __this /* static, unused */, String_t* ___eventName0, String_t* ___eventValues1, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*, char*);

	// Marshaling of parameter '___eventName0' to native representation
	char* ____eventName0_marshaled = NULL;
	____eventName0_marshaled = il2cpp_codegen_marshal_string(___eventName0);

	// Marshaling of parameter '___eventValues1' to native representation
	char* ____eventValues1_marshaled = NULL;
	____eventValues1_marshaled = il2cpp_codegen_marshal_string(___eventValues1);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(mTrackRichEvent)(____eventName0_marshaled, ____eventValues1_marshaled);

	// Marshaling cleanup of parameter '___eventName0' native representation
	il2cpp_codegen_marshal_free(____eventName0_marshaled);
	____eventName0_marshaled = NULL;

	// Marshaling cleanup of parameter '___eventValues1' native representation
	il2cpp_codegen_marshal_free(____eventValues1_marshaled);
	____eventValues1_marshaled = NULL;

}
extern "C" void DEFAULT_CALL mValidateReceipt(char*, char*, char*, char*, char*);
// System.Void AppsFlyer::mValidateReceipt(System.String,System.String,System.String,System.String,System.String)
extern "C"  void AppsFlyer_mValidateReceipt_m2008272410 (Il2CppObject * __this /* static, unused */, String_t* ___productIdentifier0, String_t* ___price1, String_t* ___currency2, String_t* ___transactionId3, String_t* ___additionalParams4, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*, char*, char*, char*, char*);

	// Marshaling of parameter '___productIdentifier0' to native representation
	char* ____productIdentifier0_marshaled = NULL;
	____productIdentifier0_marshaled = il2cpp_codegen_marshal_string(___productIdentifier0);

	// Marshaling of parameter '___price1' to native representation
	char* ____price1_marshaled = NULL;
	____price1_marshaled = il2cpp_codegen_marshal_string(___price1);

	// Marshaling of parameter '___currency2' to native representation
	char* ____currency2_marshaled = NULL;
	____currency2_marshaled = il2cpp_codegen_marshal_string(___currency2);

	// Marshaling of parameter '___transactionId3' to native representation
	char* ____transactionId3_marshaled = NULL;
	____transactionId3_marshaled = il2cpp_codegen_marshal_string(___transactionId3);

	// Marshaling of parameter '___additionalParams4' to native representation
	char* ____additionalParams4_marshaled = NULL;
	____additionalParams4_marshaled = il2cpp_codegen_marshal_string(___additionalParams4);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(mValidateReceipt)(____productIdentifier0_marshaled, ____price1_marshaled, ____currency2_marshaled, ____transactionId3_marshaled, ____additionalParams4_marshaled);

	// Marshaling cleanup of parameter '___productIdentifier0' native representation
	il2cpp_codegen_marshal_free(____productIdentifier0_marshaled);
	____productIdentifier0_marshaled = NULL;

	// Marshaling cleanup of parameter '___price1' native representation
	il2cpp_codegen_marshal_free(____price1_marshaled);
	____price1_marshaled = NULL;

	// Marshaling cleanup of parameter '___currency2' native representation
	il2cpp_codegen_marshal_free(____currency2_marshaled);
	____currency2_marshaled = NULL;

	// Marshaling cleanup of parameter '___transactionId3' native representation
	il2cpp_codegen_marshal_free(____transactionId3_marshaled);
	____transactionId3_marshaled = NULL;

	// Marshaling cleanup of parameter '___additionalParams4' native representation
	il2cpp_codegen_marshal_free(____additionalParams4_marshaled);
	____additionalParams4_marshaled = NULL;

}
extern "C" void DEFAULT_CALL mSetIsDebug(int32_t);
// System.Void AppsFlyer::mSetIsDebug(System.Boolean)
extern "C"  void AppsFlyer_mSetIsDebug_m3555622242 (Il2CppObject * __this /* static, unused */, bool ___isDebug0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(mSetIsDebug)(static_cast<int32_t>(___isDebug0));

}
extern "C" void DEFAULT_CALL mSetIsSandbox(int32_t);
// System.Void AppsFlyer::mSetIsSandbox(System.Boolean)
extern "C"  void AppsFlyer_mSetIsSandbox_m2595239082 (Il2CppObject * __this /* static, unused */, bool ___isSandbox0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(mSetIsSandbox)(static_cast<int32_t>(___isSandbox0));

}
extern "C" void DEFAULT_CALL mGetConversionData();
// System.Void AppsFlyer::mGetConversionData()
extern "C"  void AppsFlyer_mGetConversionData_m181512626 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(mGetConversionData)();

}
extern "C" void DEFAULT_CALL mHandleOpenUrl(char*, char*, char*);
// System.Void AppsFlyer::mHandleOpenUrl(System.String,System.String,System.String)
extern "C"  void AppsFlyer_mHandleOpenUrl_m456505413 (Il2CppObject * __this /* static, unused */, String_t* ___url0, String_t* ___sourceApplication1, String_t* ___annotation2, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*, char*, char*);

	// Marshaling of parameter '___url0' to native representation
	char* ____url0_marshaled = NULL;
	____url0_marshaled = il2cpp_codegen_marshal_string(___url0);

	// Marshaling of parameter '___sourceApplication1' to native representation
	char* ____sourceApplication1_marshaled = NULL;
	____sourceApplication1_marshaled = il2cpp_codegen_marshal_string(___sourceApplication1);

	// Marshaling of parameter '___annotation2' to native representation
	char* ____annotation2_marshaled = NULL;
	____annotation2_marshaled = il2cpp_codegen_marshal_string(___annotation2);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(mHandleOpenUrl)(____url0_marshaled, ____sourceApplication1_marshaled, ____annotation2_marshaled);

	// Marshaling cleanup of parameter '___url0' native representation
	il2cpp_codegen_marshal_free(____url0_marshaled);
	____url0_marshaled = NULL;

	// Marshaling cleanup of parameter '___sourceApplication1' native representation
	il2cpp_codegen_marshal_free(____sourceApplication1_marshaled);
	____sourceApplication1_marshaled = NULL;

	// Marshaling cleanup of parameter '___annotation2' native representation
	il2cpp_codegen_marshal_free(____annotation2_marshaled);
	____annotation2_marshaled = NULL;

}
extern "C" char* DEFAULT_CALL mGetAppsFlyerId();
// System.String AppsFlyer::mGetAppsFlyerId()
extern "C"  String_t* AppsFlyer_mGetAppsFlyerId_m3223677698 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef char* (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	char* returnValue = reinterpret_cast<PInvokeFunc>(mGetAppsFlyerId)();

	// Marshaling of return value back from native representation
	String_t* _returnValue_unmarshaled = NULL;
	_returnValue_unmarshaled = il2cpp_codegen_marshal_string_result(returnValue);

	// Marshaling cleanup of return value native representation
	il2cpp_codegen_marshal_free(returnValue);
	returnValue = NULL;

	return _returnValue_unmarshaled;
}
extern "C" void DEFAULT_CALL mHandlePushNotification(char*);
// System.Void AppsFlyer::mHandlePushNotification(System.String)
extern "C"  void AppsFlyer_mHandlePushNotification_m1207167649 (Il2CppObject * __this /* static, unused */, String_t* ___payload0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*);

	// Marshaling of parameter '___payload0' to native representation
	char* ____payload0_marshaled = NULL;
	____payload0_marshaled = il2cpp_codegen_marshal_string(___payload0);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(mHandlePushNotification)(____payload0_marshaled);

	// Marshaling cleanup of parameter '___payload0' native representation
	il2cpp_codegen_marshal_free(____payload0_marshaled);
	____payload0_marshaled = NULL;

}
extern "C" void DEFAULT_CALL mRegisterUninstall(uint8_t*);
// System.Void AppsFlyer::mRegisterUninstall(System.Byte[])
extern "C"  void AppsFlyer_mRegisterUninstall_m538787490 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___pushToken0, const MethodInfo* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (uint8_t*);

	// Marshaling of parameter '___pushToken0' to native representation
	uint8_t* ____pushToken0_marshaled = NULL;
	if (___pushToken0 != NULL)
	{
		____pushToken0_marshaled = reinterpret_cast<uint8_t*>((___pushToken0)->GetAddressAtUnchecked(0));
	}

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(mRegisterUninstall)(____pushToken0_marshaled);

}
// System.Void AppsFlyer::trackEvent(System.String,System.String)
extern "C"  void AppsFlyer_trackEvent_m3353671724 (Il2CppObject * __this /* static, unused */, String_t* ___eventName0, String_t* ___eventValue1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AppsFlyer_trackEvent_m3353671724_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// mTrackEvent(eventName,eventValue);
		String_t* L_0 = ___eventName0;
		String_t* L_1 = ___eventValue1;
		// mTrackEvent(eventName,eventValue);
		AppsFlyer_mTrackEvent_m2869132329(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		// print("AF.cs this is deprecated method. please use trackRichEvent instead.");
		// print("AF.cs this is deprecated method. please use trackRichEvent instead.");
		MonoBehaviour_print_m3437620292(NULL /*static, unused*/, _stringLiteral3457038533, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void AppsFlyer::setCurrencyCode(System.String)
extern "C"  void AppsFlyer_setCurrencyCode_m2729779557 (Il2CppObject * __this /* static, unused */, String_t* ___currencyCode0, const MethodInfo* method)
{
	{
		// mSetCurrencyCode(currencyCode);
		String_t* L_0 = ___currencyCode0;
		// mSetCurrencyCode(currencyCode);
		AppsFlyer_mSetCurrencyCode_m2952676942(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void AppsFlyer::setCustomerUserID(System.String)
extern "C"  void AppsFlyer_setCustomerUserID_m1797548495 (Il2CppObject * __this /* static, unused */, String_t* ___customerUserID0, const MethodInfo* method)
{
	{
		// mSetCustomerUserID(customerUserID);
		String_t* L_0 = ___customerUserID0;
		// mSetCustomerUserID(customerUserID);
		AppsFlyer_mSetCustomerUserID_m4292130114(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void AppsFlyer::setAppsFlyerKey(System.String)
extern "C"  void AppsFlyer_setAppsFlyerKey_m1791358164 (Il2CppObject * __this /* static, unused */, String_t* ___key0, const MethodInfo* method)
{
	{
		// mSetAppsFlyerDevKey(key);
		String_t* L_0 = ___key0;
		// mSetAppsFlyerDevKey(key);
		AppsFlyer_mSetAppsFlyerDevKey_m3489814122(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void AppsFlyer::trackAppLaunch()
extern "C"  void AppsFlyer_trackAppLaunch_m1057024514 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		// mTrackAppLaunch();
		AppsFlyer_mTrackAppLaunch_m1688197437(NULL /*static, unused*/, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void AppsFlyer::setAppID(System.String)
extern "C"  void AppsFlyer_setAppID_m3577374119 (Il2CppObject * __this /* static, unused */, String_t* ___appleAppId0, const MethodInfo* method)
{
	{
		// mSetAppID(appleAppId);
		String_t* L_0 = ___appleAppId0;
		// mSetAppID(appleAppId);
		AppsFlyer_mSetAppID_m3092783132(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void AppsFlyer::trackRichEvent(System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern "C"  void AppsFlyer_trackRichEvent_m3152788389 (Il2CppObject * __this /* static, unused */, String_t* ___eventName0, Dictionary_2_t3943999495 * ___eventValues1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AppsFlyer_trackRichEvent_m3152788389_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	KeyValuePair_2_t1701344717  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Enumerator_t969056901  V_2;
	memset(&V_2, 0, sizeof(V_2));
	String_t* V_3 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		// string attributesString = "";
		V_0 = _stringLiteral371857150;
		// foreach(KeyValuePair<string, string> kvp in eventValues)
		Dictionary_2_t3943999495 * L_0 = ___eventValues1;
		// foreach(KeyValuePair<string, string> kvp in eventValues)
		NullCheck(L_0);
		Enumerator_t969056901  L_1 = Dictionary_2_GetEnumerator_m2456297074(L_0, /*hidden argument*/Dictionary_2_GetEnumerator_m2456297074_MethodInfo_var);
		V_2 = L_1;
	}

IL_000f:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0054;
		}

IL_0014:
		{
			// foreach(KeyValuePair<string, string> kvp in eventValues)
			// foreach(KeyValuePair<string, string> kvp in eventValues)
			KeyValuePair_2_t1701344717  L_2 = Enumerator_get_Current_m4048655796((&V_2), /*hidden argument*/Enumerator_get_Current_m4048655796_MethodInfo_var);
			V_1 = L_2;
			// attributesString += kvp.Key + "=" + kvp.Value + "\n";
			String_t* L_3 = V_0;
			V_3 = L_3;
			StringU5BU5D_t1642385972* L_4 = ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)5));
			String_t* L_5 = V_3;
			NullCheck(L_4);
			ArrayElementTypeCheck (L_4, L_5);
			(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)L_5);
			StringU5BU5D_t1642385972* L_6 = L_4;
			// attributesString += kvp.Key + "=" + kvp.Value + "\n";
			String_t* L_7 = KeyValuePair_2_get_Key_m2641560888((&V_1), /*hidden argument*/KeyValuePair_2_get_Key_m2641560888_MethodInfo_var);
			NullCheck(L_6);
			ArrayElementTypeCheck (L_6, L_7);
			(L_6)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_7);
			StringU5BU5D_t1642385972* L_8 = L_6;
			NullCheck(L_8);
			ArrayElementTypeCheck (L_8, _stringLiteral372029329);
			(L_8)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral372029329);
			StringU5BU5D_t1642385972* L_9 = L_8;
			// attributesString += kvp.Key + "=" + kvp.Value + "\n";
			String_t* L_10 = KeyValuePair_2_get_Value_m2909389725((&V_1), /*hidden argument*/KeyValuePair_2_get_Value_m2909389725_MethodInfo_var);
			NullCheck(L_9);
			ArrayElementTypeCheck (L_9, L_10);
			(L_9)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_10);
			StringU5BU5D_t1642385972* L_11 = L_9;
			NullCheck(L_11);
			ArrayElementTypeCheck (L_11, _stringLiteral372029352);
			(L_11)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral372029352);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_12 = String_Concat_m626692867(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
			V_0 = L_12;
		}

IL_0054:
		{
			// foreach(KeyValuePair<string, string> kvp in eventValues)
			bool L_13 = Enumerator_MoveNext_m52089307((&V_2), /*hidden argument*/Enumerator_MoveNext_m52089307_MethodInfo_var);
			if (L_13)
			{
				goto IL_0014;
			}
		}

IL_0060:
		{
			IL2CPP_LEAVE(0x73, FINALLY_0065);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0065;
	}

FINALLY_0065:
	{ // begin finally (depth: 1)
		// foreach(KeyValuePair<string, string> kvp in eventValues)
		Enumerator_Dispose_m882561911((&V_2), /*hidden argument*/Enumerator_Dispose_m882561911_MethodInfo_var);
		IL2CPP_END_FINALLY(101)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(101)
	{
		IL2CPP_JUMP_TBL(0x73, IL_0073)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0073:
	{
		// mTrackRichEvent (eventName, attributesString);
		String_t* L_14 = ___eventName0;
		String_t* L_15 = V_0;
		// mTrackRichEvent (eventName, attributesString);
		AppsFlyer_mTrackRichEvent_m3339608949(NULL /*static, unused*/, L_14, L_15, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void AppsFlyer::validateReceipt(System.String,System.String,System.String,System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern "C"  void AppsFlyer_validateReceipt_m926842288 (Il2CppObject * __this /* static, unused */, String_t* ___productIdentifier0, String_t* ___price1, String_t* ___currency2, String_t* ___transactionId3, Dictionary_2_t3943999495 * ___additionalParametes4, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AppsFlyer_validateReceipt_m926842288_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	KeyValuePair_2_t1701344717  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Enumerator_t969056901  V_2;
	memset(&V_2, 0, sizeof(V_2));
	String_t* V_3 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		// string attributesString = "";
		V_0 = _stringLiteral371857150;
		// foreach(KeyValuePair<string, string> kvp in additionalParametes)
		Dictionary_2_t3943999495 * L_0 = ___additionalParametes4;
		// foreach(KeyValuePair<string, string> kvp in additionalParametes)
		NullCheck(L_0);
		Enumerator_t969056901  L_1 = Dictionary_2_GetEnumerator_m2456297074(L_0, /*hidden argument*/Dictionary_2_GetEnumerator_m2456297074_MethodInfo_var);
		V_2 = L_1;
	}

IL_0010:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0055;
		}

IL_0015:
		{
			// foreach(KeyValuePair<string, string> kvp in additionalParametes)
			// foreach(KeyValuePair<string, string> kvp in additionalParametes)
			KeyValuePair_2_t1701344717  L_2 = Enumerator_get_Current_m4048655796((&V_2), /*hidden argument*/Enumerator_get_Current_m4048655796_MethodInfo_var);
			V_1 = L_2;
			// attributesString += kvp.Key + "=" + kvp.Value + "\n";
			String_t* L_3 = V_0;
			V_3 = L_3;
			StringU5BU5D_t1642385972* L_4 = ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)5));
			String_t* L_5 = V_3;
			NullCheck(L_4);
			ArrayElementTypeCheck (L_4, L_5);
			(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)L_5);
			StringU5BU5D_t1642385972* L_6 = L_4;
			// attributesString += kvp.Key + "=" + kvp.Value + "\n";
			String_t* L_7 = KeyValuePair_2_get_Key_m2641560888((&V_1), /*hidden argument*/KeyValuePair_2_get_Key_m2641560888_MethodInfo_var);
			NullCheck(L_6);
			ArrayElementTypeCheck (L_6, L_7);
			(L_6)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_7);
			StringU5BU5D_t1642385972* L_8 = L_6;
			NullCheck(L_8);
			ArrayElementTypeCheck (L_8, _stringLiteral372029329);
			(L_8)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral372029329);
			StringU5BU5D_t1642385972* L_9 = L_8;
			// attributesString += kvp.Key + "=" + kvp.Value + "\n";
			String_t* L_10 = KeyValuePair_2_get_Value_m2909389725((&V_1), /*hidden argument*/KeyValuePair_2_get_Value_m2909389725_MethodInfo_var);
			NullCheck(L_9);
			ArrayElementTypeCheck (L_9, L_10);
			(L_9)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_10);
			StringU5BU5D_t1642385972* L_11 = L_9;
			NullCheck(L_11);
			ArrayElementTypeCheck (L_11, _stringLiteral372029352);
			(L_11)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral372029352);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_12 = String_Concat_m626692867(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
			V_0 = L_12;
		}

IL_0055:
		{
			// foreach(KeyValuePair<string, string> kvp in additionalParametes)
			bool L_13 = Enumerator_MoveNext_m52089307((&V_2), /*hidden argument*/Enumerator_MoveNext_m52089307_MethodInfo_var);
			if (L_13)
			{
				goto IL_0015;
			}
		}

IL_0061:
		{
			IL2CPP_LEAVE(0x74, FINALLY_0066);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0066;
	}

FINALLY_0066:
	{ // begin finally (depth: 1)
		// foreach(KeyValuePair<string, string> kvp in additionalParametes)
		Enumerator_Dispose_m882561911((&V_2), /*hidden argument*/Enumerator_Dispose_m882561911_MethodInfo_var);
		IL2CPP_END_FINALLY(102)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(102)
	{
		IL2CPP_JUMP_TBL(0x74, IL_0074)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0074:
	{
		// mValidateReceipt (productIdentifier, price, currency, transactionId, attributesString);
		String_t* L_14 = ___productIdentifier0;
		String_t* L_15 = ___price1;
		String_t* L_16 = ___currency2;
		String_t* L_17 = ___transactionId3;
		String_t* L_18 = V_0;
		// mValidateReceipt (productIdentifier, price, currency, transactionId, attributesString);
		AppsFlyer_mValidateReceipt_m2008272410(NULL /*static, unused*/, L_14, L_15, L_16, L_17, L_18, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void AppsFlyer::setIsDebug(System.Boolean)
extern "C"  void AppsFlyer_setIsDebug_m735350137 (Il2CppObject * __this /* static, unused */, bool ___isDebug0, const MethodInfo* method)
{
	{
		// mSetIsDebug(isDebug);
		bool L_0 = ___isDebug0;
		// mSetIsDebug(isDebug);
		AppsFlyer_mSetIsDebug_m3555622242(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void AppsFlyer::setIsSandbox(System.Boolean)
extern "C"  void AppsFlyer_setIsSandbox_m23520209 (Il2CppObject * __this /* static, unused */, bool ___isSandbox0, const MethodInfo* method)
{
	{
		// mSetIsSandbox(isSandbox);
		bool L_0 = ___isSandbox0;
		// mSetIsSandbox(isSandbox);
		AppsFlyer_mSetIsSandbox_m2595239082(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void AppsFlyer::getConversionData()
extern "C"  void AppsFlyer_getConversionData_m2632946641 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		// mGetConversionData ();
		AppsFlyer_mGetConversionData_m181512626(NULL /*static, unused*/, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.String AppsFlyer::getAppsFlyerId()
extern "C"  String_t* AppsFlyer_getAppsFlyerId_m3568123195 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	String_t* V_0 = NULL;
	{
		// return mGetAppsFlyerId ();
		String_t* L_0 = AppsFlyer_mGetAppsFlyerId_m3223677698(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		// }
		String_t* L_1 = V_0;
		return L_1;
	}
}
// System.Void AppsFlyer::handleOpenUrl(System.String,System.String,System.String)
extern "C"  void AppsFlyer_handleOpenUrl_m1091647522 (Il2CppObject * __this /* static, unused */, String_t* ___url0, String_t* ___sourceApplication1, String_t* ___annotation2, const MethodInfo* method)
{
	{
		// mHandleOpenUrl (url, sourceApplication, annotation);
		String_t* L_0 = ___url0;
		String_t* L_1 = ___sourceApplication1;
		String_t* L_2 = ___annotation2;
		// mHandleOpenUrl (url, sourceApplication, annotation);
		AppsFlyer_mHandleOpenUrl_m456505413(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void AppsFlyer::handlePushNotification(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern "C"  void AppsFlyer_handlePushNotification_m3382247817 (Il2CppObject * __this /* static, unused */, Dictionary_2_t3943999495 * ___payload0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AppsFlyer_handlePushNotification_m3382247817_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	KeyValuePair_2_t1701344717  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Enumerator_t969056901  V_2;
	memset(&V_2, 0, sizeof(V_2));
	String_t* V_3 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		// string attributesString = "";
		V_0 = _stringLiteral371857150;
		// foreach(KeyValuePair<string, string> kvp in payload) {
		Dictionary_2_t3943999495 * L_0 = ___payload0;
		// foreach(KeyValuePair<string, string> kvp in payload) {
		NullCheck(L_0);
		Enumerator_t969056901  L_1 = Dictionary_2_GetEnumerator_m2456297074(L_0, /*hidden argument*/Dictionary_2_GetEnumerator_m2456297074_MethodInfo_var);
		V_2 = L_1;
	}

IL_000f:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0054;
		}

IL_0014:
		{
			// foreach(KeyValuePair<string, string> kvp in payload) {
			// foreach(KeyValuePair<string, string> kvp in payload) {
			KeyValuePair_2_t1701344717  L_2 = Enumerator_get_Current_m4048655796((&V_2), /*hidden argument*/Enumerator_get_Current_m4048655796_MethodInfo_var);
			V_1 = L_2;
			// attributesString += kvp.Key + "=" + kvp.Value + "\n";
			String_t* L_3 = V_0;
			V_3 = L_3;
			StringU5BU5D_t1642385972* L_4 = ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)5));
			String_t* L_5 = V_3;
			NullCheck(L_4);
			ArrayElementTypeCheck (L_4, L_5);
			(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)L_5);
			StringU5BU5D_t1642385972* L_6 = L_4;
			// attributesString += kvp.Key + "=" + kvp.Value + "\n";
			String_t* L_7 = KeyValuePair_2_get_Key_m2641560888((&V_1), /*hidden argument*/KeyValuePair_2_get_Key_m2641560888_MethodInfo_var);
			NullCheck(L_6);
			ArrayElementTypeCheck (L_6, L_7);
			(L_6)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_7);
			StringU5BU5D_t1642385972* L_8 = L_6;
			NullCheck(L_8);
			ArrayElementTypeCheck (L_8, _stringLiteral372029329);
			(L_8)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral372029329);
			StringU5BU5D_t1642385972* L_9 = L_8;
			// attributesString += kvp.Key + "=" + kvp.Value + "\n";
			String_t* L_10 = KeyValuePair_2_get_Value_m2909389725((&V_1), /*hidden argument*/KeyValuePair_2_get_Value_m2909389725_MethodInfo_var);
			NullCheck(L_9);
			ArrayElementTypeCheck (L_9, L_10);
			(L_9)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_10);
			StringU5BU5D_t1642385972* L_11 = L_9;
			NullCheck(L_11);
			ArrayElementTypeCheck (L_11, _stringLiteral372029352);
			(L_11)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral372029352);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_12 = String_Concat_m626692867(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
			V_0 = L_12;
		}

IL_0054:
		{
			// foreach(KeyValuePair<string, string> kvp in payload) {
			bool L_13 = Enumerator_MoveNext_m52089307((&V_2), /*hidden argument*/Enumerator_MoveNext_m52089307_MethodInfo_var);
			if (L_13)
			{
				goto IL_0014;
			}
		}

IL_0060:
		{
			IL2CPP_LEAVE(0x73, FINALLY_0065);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0065;
	}

FINALLY_0065:
	{ // begin finally (depth: 1)
		// foreach(KeyValuePair<string, string> kvp in payload) {
		Enumerator_Dispose_m882561911((&V_2), /*hidden argument*/Enumerator_Dispose_m882561911_MethodInfo_var);
		IL2CPP_END_FINALLY(101)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(101)
	{
		IL2CPP_JUMP_TBL(0x73, IL_0073)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0073:
	{
		// mHandlePushNotification(attributesString);
		String_t* L_14 = V_0;
		// mHandlePushNotification(attributesString);
		AppsFlyer_mHandlePushNotification_m1207167649(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void AppsFlyer::registerUninstall(System.Byte[])
extern "C"  void AppsFlyer_registerUninstall_m664703751 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___token0, const MethodInfo* method)
{
	{
		// mRegisterUninstall(token);
		ByteU5BU5D_t3397334013* L_0 = ___token0;
		// mRegisterUninstall(token);
		AppsFlyer_mRegisterUninstall_m538787490(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void AppsFlyerTrackerCallbacks::.ctor()
extern "C"  void AppsFlyerTrackerCallbacks__ctor_m1914119225 (AppsFlyerTrackerCallbacks_t3562121710 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AppsFlyerTrackerCallbacks::Start()
extern "C"  void AppsFlyerTrackerCallbacks_Start_m2082414881 (AppsFlyerTrackerCallbacks_t3562121710 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AppsFlyerTrackerCallbacks_Start_m2082414881_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// print ("AppsFlyerTrackerCallbacks on Start");
		// print ("AppsFlyerTrackerCallbacks on Start");
		MonoBehaviour_print_m3437620292(NULL /*static, unused*/, _stringLiteral2850019861, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void AppsFlyerTrackerCallbacks::Update()
extern "C"  void AppsFlyerTrackerCallbacks_Update_m3628352294 (AppsFlyerTrackerCallbacks_t3562121710 * __this, const MethodInfo* method)
{
	{
		// }
		return;
	}
}
// System.Void AppsFlyerTrackerCallbacks::didReceiveConversionData(System.String)
extern "C"  void AppsFlyerTrackerCallbacks_didReceiveConversionData_m3717905989 (AppsFlyerTrackerCallbacks_t3562121710 * __this, String_t* ___conversionData0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AppsFlyerTrackerCallbacks_didReceiveConversionData_m3717905989_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// printCallback ("AppsFlyerTrackerCallbacks:: got conversion data = " + conversionData);
		String_t* L_0 = ___conversionData0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral1259047537, L_0, /*hidden argument*/NULL);
		// printCallback ("AppsFlyerTrackerCallbacks:: got conversion data = " + conversionData);
		AppsFlyerTrackerCallbacks_printCallback_m2540129811(__this, L_1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void AppsFlyerTrackerCallbacks::didReceiveConversionDataWithError(System.String)
extern "C"  void AppsFlyerTrackerCallbacks_didReceiveConversionDataWithError_m3101201123 (AppsFlyerTrackerCallbacks_t3562121710 * __this, String_t* ___error0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AppsFlyerTrackerCallbacks_didReceiveConversionDataWithError_m3101201123_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// printCallback ("AppsFlyerTrackerCallbacks:: got conversion data error = " + error);
		String_t* L_0 = ___error0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral1294667269, L_0, /*hidden argument*/NULL);
		// printCallback ("AppsFlyerTrackerCallbacks:: got conversion data error = " + error);
		AppsFlyerTrackerCallbacks_printCallback_m2540129811(__this, L_1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void AppsFlyerTrackerCallbacks::didFinishValidateReceipt(System.String)
extern "C"  void AppsFlyerTrackerCallbacks_didFinishValidateReceipt_m1223936711 (AppsFlyerTrackerCallbacks_t3562121710 * __this, String_t* ___validateResult0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AppsFlyerTrackerCallbacks_didFinishValidateReceipt_m1223936711_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// printCallback ("AppsFlyerTrackerCallbacks:: got didFinishValidateReceipt  = " + validateResult);
		String_t* L_0 = ___validateResult0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral4209498249, L_0, /*hidden argument*/NULL);
		// printCallback ("AppsFlyerTrackerCallbacks:: got didFinishValidateReceipt  = " + validateResult);
		AppsFlyerTrackerCallbacks_printCallback_m2540129811(__this, L_1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void AppsFlyerTrackerCallbacks::didFinishValidateReceiptWithError(System.String)
extern "C"  void AppsFlyerTrackerCallbacks_didFinishValidateReceiptWithError_m803271077 (AppsFlyerTrackerCallbacks_t3562121710 * __this, String_t* ___error0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AppsFlyerTrackerCallbacks_didFinishValidateReceiptWithError_m803271077_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// printCallback ("AppsFlyerTrackerCallbacks:: got idFinishValidateReceiptWithError error = " + error);
		String_t* L_0 = ___error0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral1909124487, L_0, /*hidden argument*/NULL);
		// printCallback ("AppsFlyerTrackerCallbacks:: got idFinishValidateReceiptWithError error = " + error);
		AppsFlyerTrackerCallbacks_printCallback_m2540129811(__this, L_1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void AppsFlyerTrackerCallbacks::onAppOpenAttribution(System.String)
extern "C"  void AppsFlyerTrackerCallbacks_onAppOpenAttribution_m4211417896 (AppsFlyerTrackerCallbacks_t3562121710 * __this, String_t* ___validateResult0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AppsFlyerTrackerCallbacks_onAppOpenAttribution_m4211417896_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// printCallback ("AppsFlyerTrackerCallbacks:: got onAppOpenAttribution  = " + validateResult);
		String_t* L_0 = ___validateResult0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral184702546, L_0, /*hidden argument*/NULL);
		// printCallback ("AppsFlyerTrackerCallbacks:: got onAppOpenAttribution  = " + validateResult);
		AppsFlyerTrackerCallbacks_printCallback_m2540129811(__this, L_1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void AppsFlyerTrackerCallbacks::onAppOpenAttributionFailure(System.String)
extern "C"  void AppsFlyerTrackerCallbacks_onAppOpenAttributionFailure_m1384204616 (AppsFlyerTrackerCallbacks_t3562121710 * __this, String_t* ___error0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AppsFlyerTrackerCallbacks_onAppOpenAttributionFailure_m1384204616_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// printCallback ("AppsFlyerTrackerCallbacks:: got onAppOpenAttributionFailure error = " + error);
		String_t* L_0 = ___error0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral3688921710, L_0, /*hidden argument*/NULL);
		// printCallback ("AppsFlyerTrackerCallbacks:: got onAppOpenAttributionFailure error = " + error);
		AppsFlyerTrackerCallbacks_printCallback_m2540129811(__this, L_1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void AppsFlyerTrackerCallbacks::onInAppBillingSuccess()
extern "C"  void AppsFlyerTrackerCallbacks_onInAppBillingSuccess_m2018164444 (AppsFlyerTrackerCallbacks_t3562121710 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AppsFlyerTrackerCallbacks_onInAppBillingSuccess_m2018164444_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// printCallback ("AppsFlyerTrackerCallbacks:: got onInAppBillingSuccess succcess");
		// printCallback ("AppsFlyerTrackerCallbacks:: got onInAppBillingSuccess succcess");
		AppsFlyerTrackerCallbacks_printCallback_m2540129811(__this, _stringLiteral4055123575, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void AppsFlyerTrackerCallbacks::onInAppBillingFailure(System.String)
extern "C"  void AppsFlyerTrackerCallbacks_onInAppBillingFailure_m123106739 (AppsFlyerTrackerCallbacks_t3562121710 * __this, String_t* ___error0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AppsFlyerTrackerCallbacks_onInAppBillingFailure_m123106739_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// printCallback ("AppsFlyerTrackerCallbacks:: got onInAppBillingFailure error = " + error);
		String_t* L_0 = ___error0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral3776751665, L_0, /*hidden argument*/NULL);
		// printCallback ("AppsFlyerTrackerCallbacks:: got onInAppBillingFailure error = " + error);
		AppsFlyerTrackerCallbacks_printCallback_m2540129811(__this, L_1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void AppsFlyerTrackerCallbacks::printCallback(System.String)
extern "C"  void AppsFlyerTrackerCallbacks_printCallback_m2540129811 (AppsFlyerTrackerCallbacks_t3562121710 * __this, String_t* ___str0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AppsFlyerTrackerCallbacks_printCallback_m2540129811_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// callbacks.text += str + "\n";
		Text_t356221433 * L_0 = __this->get_callbacks_2();
		Text_t356221433 * L_1 = L_0;
		// callbacks.text += str + "\n";
		NullCheck(L_1);
		String_t* L_2 = VirtFuncInvoker0< String_t* >::Invoke(71 /* System.String UnityEngine.UI.Text::get_text() */, L_1);
		String_t* L_3 = ___str0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m612901809(NULL /*static, unused*/, L_2, L_3, _stringLiteral372029352, /*hidden argument*/NULL);
		// callbacks.text += str + "\n";
		NullCheck(L_1);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_1, L_4);
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
