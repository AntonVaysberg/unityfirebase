﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.GameObject
struct GameObject_t1756533147;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AF_Sample_BGScript
struct  AF_Sample_BGScript_t3397449209  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject AF_Sample_BGScript::Button
	GameObject_t1756533147 * ___Button_2;
	// UnityEngine.GameObject AF_Sample_BGScript::ButtonText
	GameObject_t1756533147 * ___ButtonText_3;
	// UnityEngine.GameObject AF_Sample_BGScript::Check
	GameObject_t1756533147 * ___Check_4;
	// UnityEngine.GameObject AF_Sample_BGScript::text
	GameObject_t1756533147 * ___text_5;

public:
	inline static int32_t get_offset_of_Button_2() { return static_cast<int32_t>(offsetof(AF_Sample_BGScript_t3397449209, ___Button_2)); }
	inline GameObject_t1756533147 * get_Button_2() const { return ___Button_2; }
	inline GameObject_t1756533147 ** get_address_of_Button_2() { return &___Button_2; }
	inline void set_Button_2(GameObject_t1756533147 * value)
	{
		___Button_2 = value;
		Il2CppCodeGenWriteBarrier(&___Button_2, value);
	}

	inline static int32_t get_offset_of_ButtonText_3() { return static_cast<int32_t>(offsetof(AF_Sample_BGScript_t3397449209, ___ButtonText_3)); }
	inline GameObject_t1756533147 * get_ButtonText_3() const { return ___ButtonText_3; }
	inline GameObject_t1756533147 ** get_address_of_ButtonText_3() { return &___ButtonText_3; }
	inline void set_ButtonText_3(GameObject_t1756533147 * value)
	{
		___ButtonText_3 = value;
		Il2CppCodeGenWriteBarrier(&___ButtonText_3, value);
	}

	inline static int32_t get_offset_of_Check_4() { return static_cast<int32_t>(offsetof(AF_Sample_BGScript_t3397449209, ___Check_4)); }
	inline GameObject_t1756533147 * get_Check_4() const { return ___Check_4; }
	inline GameObject_t1756533147 ** get_address_of_Check_4() { return &___Check_4; }
	inline void set_Check_4(GameObject_t1756533147 * value)
	{
		___Check_4 = value;
		Il2CppCodeGenWriteBarrier(&___Check_4, value);
	}

	inline static int32_t get_offset_of_text_5() { return static_cast<int32_t>(offsetof(AF_Sample_BGScript_t3397449209, ___text_5)); }
	inline GameObject_t1756533147 * get_text_5() const { return ___text_5; }
	inline GameObject_t1756533147 ** get_address_of_text_5() { return &___text_5; }
	inline void set_text_5(GameObject_t1756533147 * value)
	{
		___text_5 = value;
		Il2CppCodeGenWriteBarrier(&___text_5, value);
	}
};

struct AF_Sample_BGScript_t3397449209_StaticFields
{
public:
	// System.Boolean AF_Sample_BGScript::isPressed
	bool ___isPressed_6;

public:
	inline static int32_t get_offset_of_isPressed_6() { return static_cast<int32_t>(offsetof(AF_Sample_BGScript_t3397449209_StaticFields, ___isPressed_6)); }
	inline bool get_isPressed_6() const { return ___isPressed_6; }
	inline bool* get_address_of_isPressed_6() { return &___isPressed_6; }
	inline void set_isPressed_6(bool value)
	{
		___isPressed_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
