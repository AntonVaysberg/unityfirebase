﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System.Text;
using Firebase.Messaging;
using Firebase.Unity;


public class StartUp : MonoBehaviour {
	public GameObject text;
	private bool tokenSent;

	// Use this for initialization
	void Start () {
		Application.runInBackground = true;
		Screen.orientation = ScreenOrientation.Portrait;
		DontDestroyOnLoad (this);
		AppsFlyer.setIsDebug(true);

		string unityEventName = "test";
		Dictionary<string, string> unityEventValues = new Dictionary<string, string> ();
		unityEventValues.Add ("testValues1", "testValues2");

		#if UNITY_IOS 
		AppsFlyer.setAppsFlyerKey ("iErwCWL8p5wapsDjnDBQQb");

		//IGG test
		AppsFlyer.trackRichEvent(unityEventName, unityEventValues);

		AppsFlyer.setAppID ("300200112");
		AppsFlyer.getConversionData ();
		AppsFlyer.trackAppLaunch ();
		AppsFlyer.setCustomerUserID ("Unity Test customerUserID");
		AppsFlyer.setCurrencyCode ("EUR");

		AppsFlyer.setIsSandbox(true);
		Dictionary<string, string> validateTest = new Dictionary<string, string>();
//		AppsFlyer.validateReceipt( "", "", "", "", validateTest );
		AppsFlyer.validateReceipt( "GSNA_CoinsPack1", "10", "USD", "410000253818916", validateTest );

		// register to push notifications for iOS uninstall
		UnityEngine.iOS.NotificationServices.RegisterForNotifications (UnityEngine.iOS.NotificationType.Alert | UnityEngine.iOS.NotificationType.Badge | UnityEngine.iOS.NotificationType.Sound);
		Screen.orientation = ScreenOrientation.Portrait;

		AppsFlyer.registerUninstall (Encoding.ASCII.GetBytes("72b0ab229608d0e0e500c1fd7df4e969303fbefbc230b45509b0c90109be49f7"));


		//IGG test
		AppsFlyer.trackRichEvent(unityEventName, unityEventValues);

		#elif UNITY_ANDROID

		string AF_ID = AppsFlyer.getAppsFlyerId();
		print (" get appsflyer id call: " + AF_ID);

		AppsFlyer.setAppID ("com.anton.unitytest"); 

		// for getting the conversion data
		AppsFlyer.loadConversionData("StartUp");


		// for in app billing validation
		 AppsFlyer.createValidateInAppListener ("AppsFlyerTrackerCallbacks", "onInAppBillingSuccess", "onInAppBillingFailure"); 


		//setCustomerUserID
		AppsFlyer.setCustomerUserID ("unity test customerID");

		AppsFlyer.init ("iErwCWL8p5wapsDjnDBQQb");
//		AppsFlyer.setCurrencyCode("GBP");

		//Firebase 
		Firebase.Messaging.FirebaseMessaging.TokenReceived += OnTokenReceived;
//		AppsFlyer.enableUninstallTracking("372032065920");

		//IGG test
		AppsFlyer.trackRichEvent(unityEventName, unityEventValues);

		Purchase();

		//		In App Purchase Receipt Validation
		string base64EncodedPublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA3dkBTr2pD2YSqSK2ewlEWwH9Llu0iA4PkwgVOyNRxOsHfrOlqi0Cm51qdNS0aqh/SMZkuQTAroqH3pAr9gVFOiejKRw+ymTaL5wB9+5n1mAbdeO2tv2FDsbawDvp7u6fIBejYt7Dtmih+kcu707fEO58HZWqgzx9qSHmrkMZr6yvHCtAhdBLwSBBjyhPHy7RAwKA+PE+HYVV2UNb5urqIZ9eI1dAv3RHX/xxHVHRJcjnTyMAqBmfFM+o31tp8/1CxGIazVN6HpVk8Qi2uqSS5HdKUu6VnIK8VuAHQbXQn4bG6GXx5Tp0SX1fKrejo7hupNUCgOlqsYHFYxsRkEOi0QIDAQAB";
		string signature = "OaIdwQOcmrJrMKUx+URVy1I6aeKYiYzflkk1zIKVSs+dDv691neCbR+jlDDzVi3jfSkfirxQISxo7Pe1uzoYbpq9wBk/pMgVjjSbpvCojhA4d/Mwsf4mtAH2LJcVNjhMQdSWvGJlzva3OSt+KQ+9/pRJ15aYT2gFn3SpGSPxNxJmHPIOlM1Lr74MejVu9rnbcSjCB/oI0W4O58p9UWSt5MgmlpqlrK5YqTi1a1VnttY9r1IXFeltwZvmPbcWcYwRHFvemwYGX86huSOFBOYRfaYo9f+DinpoUoXKQEo0JrvKD2/pzFkbUTto1d2OPo1ddaYllgsb2UEV5wwFZFnemg==";
		string purchaseData = "{\"orderId\":\"\",\"packageName\":\"com.appsflyer.testapp\",\"productId\":\"consumable\",\"purchaseTime\":1497531638107,\"purchaseState\":0,\"developerPayload\":\"1497525891514-5765886608164763986\",\"purchaseToken\":\"pfkalmpnnimamdllmincaida.AO-J1OymunlPCkDQZTf8bPcesoB0n1_ND3WFynoU91-v_R1Px46m3Q-JdRKNlxMVsP2pCTKpo1et1w1IpNVXQ8-zNpRo6a2nXP7a5fQWiDv2asL1dwJPCV8NghDHbstO084IlKo6xcgy\"}";
		Dictionary <string, string> additionalParametes = null;

		AppsFlyer.validateReceipt(base64EncodedPublicKey,  purchaseData,  signature,  "30",  "GBP",  additionalParametes);
		#endif
	}


	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.Escape)) {
			//go to background when pressing back button
			#if UNITY_ANDROID
			AndroidJavaObject activity = 
				new AndroidJavaClass("com.unity3d.player.UnityPlayer")
					.GetStatic<AndroidJavaObject>("currentActivity");
			activity.Call<bool>("moveTaskToBack", true);
			#endif
		}
			

		#if UNITY_IOS 
		if (!tokenSent) { 
			byte[] token = UnityEngine.iOS.NotificationServices.deviceToken;           
			if (token != null) {     
			//For iOS uninstall
				AppsFlyer.registerUninstall (token);
				tokenSent = true;
			}
		}    
		#endif
	}
	//A custom event tracking
	public void Purchase(){
		Dictionary<string, string> eventValue = new Dictionary<string,string> ();
		eventValue.Add("af_revenue","200");
		eventValue.Add("af_content_type","category_a");
		eventValue.Add("af_content_id","1234567");
//		eventValue.Add("af_currency","EUR");
	
//		eventValue.Add ("language", "Russian - АаБбВвГгДдЕеЁёЖжЗзИиЙйКкЛлМмНнОоПпРрСсТтУуФфХхЦцЧчШшЩщЪъЫыЬьЭэЮюЯя\nChinese - 吧爸八百北不大岛的弟地东都对多儿二方港哥个关贵国过海好很会家见叫姐京九可老李零六吗妈么没美妹们明名哪那南你您朋七起千去人认日三上谁什生师识十是四他她台天湾万王我五西息系先香想小谢姓休学也一亿英友月再张这中字\nJapanese - あいうえおはひふへほがぎぐげごきゃきゅきょぎゃぎゅぎょりゃアイウエオハヒフヘホガギグゲゴキャキュキョギャギュギョリャイェウィウェウォティトゥテュディドゥデュかきくけこまみむめもざじずぜぞしゃしゅしょじゃじゅじょりゅカキクケコマミムメモザジズゼゾシャシュショジャジュジョリュ(ヷ)(ヸ)ヴ(ヹ)(ヺ)ツァツィツェツォさしすせそやゆよだぢづでどちゃちゅちょにゃにゅにょりょサシスセソヤユヨダヂヅデドチャチュチョニャニュニョリョヴァヴィヴェヴォヴャファフィフェフォフュたちつてとらりるれろばびぶべぼひゃひゅひょびゃびゅびょぢゃタチツテトラリルレロバビブベボヒャヒュヒョビャビュビョヂャシェジェチェなにぬねのわゐんゑをぱぴぷぺぽぴゃぴゅぴょみゃみゅみょぢゅナニヌネノワヰンヱヲパピプペポピャピュピョミャミュミョヂュ\nKorean - ㄱㄴㄷㄹㅁㅂㅅㅇㅈㅊㅋㅌㅍㅎㄲㄸㅃㅆㅉㅏㅐㅑㅓㅔㅕㅗㅘㅛㅜㅠㅡㅢㅣ가각갂간갇갈갉감갑값갓갔강갖갗같갚갛개객갠갤갬갭갯갰갱갸갹걀걔거걱건걷걸걹검겁것겄겅겆겉겊겋게겐겔겜겟겠겡겨격겪견겯결겸겹겼경곁계곗고곡곤곧골곪곬곯곰곱곳공곶곺과곽관괄괌괍괏광괘괙괜괠괨괩괭괴괸괼굄굉교구국군굳굴굵굶굼굽굿궁궂궈권궐궤귀귄귈귐귑귓규균귤귬그극근귿글긁금급긋긍기긱긴긷길김깁깃깄깅깊까깍깎깐깔깜깝깠깡깢깥깨깩깬깰깸깹깻깼깽꺄꺅꺼꺽꺾껀껄껌껍껏껐껑께껜껴꼍꼬꼭꼰꼴꼼꼽꼿꽁꽂꽃꽈꽉꽌꽘꽜꽝꽤꽥꽹꾀꾄꾈꾐꾕꾜꾸꾹꾼꿀꿇꿈꿉꿋꿍꿎꿔꿘꿩꿰뀀뀌뀍뀐뀔뀜뀝뀨끄끅끈끊끌끓끔끕끗끙끝끼끽낀낄낌낍낏낐낑나낙낚난낟날낡남납낫났낭낮낯낱낳내낵낸낻낼냄냅냇냈냉냐냑냠냥냬너넉넋넌넏널넓넘넙넛넜넝넞넣네넥넨넬넴넵넷녀녁년념녑녓녔녕녘녜노녹녺논놀놈놉놋농높놓놔놨뇌뇐뇔뇜뇨뇽누눅눈눋눌눓눔눕눗눠눳눴뉘뉜뉠뉨뉩뉴늄늉느늑는늘늙늠늡능늦늪늬니닉닌닐님닙닛닝닢다닥닦단닫달닭닮닳담답닷당닺닻닿대댁댄댈댐댑댓댔댕더덕던덛덜덟덤덥덧덨덩덪덫덮데덱덴델뎀뎃뎅뎌도독돈돋돌돐돔돕돗동돛돼됀됐되된될됨됩됫됬두둑둔둘둠둡둣둥둬뒀뒈뒤뒷듀듐드득든듣들듬듭듯등디딕딘딛딜딤딥딧딨딩딪따딱딲딴딷딸땀땁땃땄땅땋때땍땐땔땜땝땟땠땡떠떡떤떨떫떰떱떳떴떵떻떼뗀뗄뗌뗏뗑또똑똔똘똥뙤뚜뚝뚤뚫뚬뚱뛰뛴뛸뜀뜁뜨뜩뜬뜯뜰뜸뜹뜻띄띈띠띤띨띰띱띵라락란랄람랍랏랐랑랗래랙랜랠램랩랫랬랭랴략량러럭런럴럼럽럿렀렁렇레렉렌렐렘렙렛렝려력련렬렴렵렷렸령례로록론롤롬롭롯롱뢰료룡루룩룬룰룸룹룻룽뤄뤼류륙륜률륨륭르륵른를름릅릇릉릎리릭린릴림립릿링마막만많맏말맑맘맙맛망맞맡맣매맥맨맬맴맵맷맸맹맺먀머먹먼멀멈멉멋멍멎멓메멕멘멜멤멥멧며멱면멸몃몄명몇모목몫몬몰몸몹못몽뫃뫼묏묘무묵묶문묻물묽뭄뭇뭉뭍뭏뭐뭔뭘뭡뭣뮈뮌뮤므믄믈믐미믹민믿밀밈밉밋밍및밑뮴바박밖반받발밝밟밤밥밧방밭배백밴밷밸뱀뱁뱃뱄뱅버벅번벋벌범법벗벘벙벚베벡벤벧벨벰벱벳벵벼벽변별볌볍볏볐병볒볕보복볶본볼봄봅봇봉봐봔봤뵈뵉뵌뵐뵘뵙뵤부북분붇불붉붐붑붓붕붙붜뷔뷰브븍븐블븜븝븟비빅빈빋빌빎빔빕빗빙빚빛빠빡빤빨빰빱빳빴빵빻빼빽뺀뺄뺌뺏뺐뺑뺘뺨뻐뻑뻔뻗뻘뻠뻣뻤뻥뻬뼈뼉뼘뼝뽀뽁뽄뽈뽐뽑뽕뾰뿀뿅뿌뿍뿐뿔뿜뿝뿡쁘쁙쁜쁠쁨쁩삐삑삔삘삠삣삥사삭삮삯산삳살삵삶삼삽삿샀상샅새색샌샏샐샘샙샛샜생샤샥샨샬샴샷샹서석섞선섣설섥섦섧섪섬섭섯섰성섶세섹센셀셈셉셋셍셑셔션셧셨셰소속솎손솔솜솝솟송솥솨솩솰쇄쇈쇗쇠쇤쇨쇰쇱쇳쇼쇽숀숄숍수숙순숟술숨숩숫숭숯숱숲숴쉐쉘쉬쉰쉴쉼쉽쉿슁슈슉슐슘슛슝스슥슨슬슭슴습슷승시식신싣실싫심십싯싱싶싸싹싼쌀쌈쌉쌋쌌쌍쌓쌔쌕쌘쌜쌤쌧쌨쌩쌰썃썅써썩썬썰썲썸썹썼썽쎄쎈쎙쏘쏙쏜쏟쏠쏨쏭쏴쏵쐐쐬쐰쐴쐼쐿쑤쑥쑨쑬쑴쑹쒀쒔쓔쓕쓰쓱쓴쓸씀씁씌씐씨씩씬씰씸씹씻씽아악안앉않알앎앓암압앗았앙앞앟애액앤앨앰앱앳앴앵야약얀얄얇얌얍얏얐양얕얗얘얜어억얶언얹얺얻얼얽엄업없엇었엉엊엌엎에엑엔엘엠엡엣엥여역엮연열엶엷염엽엾엿였영옆옇예옌옐옛오옥온올옭옮옳옴옵옷옹옺옻와왁완왈왐왑왓왔왜왝왠왱외왹왼욀욈욋욍요욕욘욥욧용우욱운욷울욹욺움웁웃웅워웍원월웜웝웟웠웡웨웩웬웰웸위윅윈윌윔윗윙유육윤율윰융윷으윽은을읊음읍응의이익인일읽잃임입잇있잉잊잎왕자작잔잖잘잠잡잣잤장잦잧재잭잰잴잼잽잿쟀쟁쟈쟉쟌쟘저적전절젊점접젓젔정젖제젝젠젤젬젭젯져젼졌조족존졸졺좀좁종좆좇좋좌좍좐좔좜좝좟좠좨좬좰좸죄죈죌죔죕죗죠죤죰죱죵주죽준줄줆줌줍줏중줘줬쥐쥔쥘쥠쥡쥬쥰쥴즈즉즌즐즘즙증지직진짇질짊짐집짓징짖짙짚짜짝짠짢짤짧짬짭짯짰짱째짹짼쨀쨈쨉쨋쨌쨍쨘쩌쩍쩐쩔쩜쩝쩡쪄쪘쪼쪽쫀쫄쫌쫍쫑쫒쫓쫘쫙쬐쬔쬘쬠쬡쭁쭈쭉쭐쭘쭙쭝쭤쮸쯔쯤쯧찌찍찐찔찜찝찡찢찧차착찬찮찯찰참찹찻찼창찾찿채책챈챌챔챕챘챙챠챤챰챱처척천철첨첩첫청체첵첸첼쳇쳐쳤초촉촌촐촘촙촛총촬최쵸추축춘출춤춥춧충춰췄췌취츄츠측츰층치칙친칠칡침칩칫칭카칵칸칼캄캅캇캉캐캑캔캘캠캡캣캥캬캭커컨컬컴컵컷컹케켁켄켈켐켑켓켕켜켠켤켬켭켯켰코콕콘콜콤콥콧콩콰콱콴콸쾀쾅쾌쾍쾡쾰쿄쿠쿡쿤쿨쿰쿳쿵쿼퀘퀭퀴퀵퀸큐크큰클큼큽킁키킥킨킬킴킵킷킹타탁탄탈탉탐탑탓탔탕태택탠탤탬탭탯탰탱터턱턴털텀텁텃텄텅테텍텐텔템텝텟텠텡텨토톡톤톨톰톱톳통퇘퇴투툭툰툴툼툽툿퉁퉤퉷튀튄튈튐튑튓튕튜튬트특튼튿틀틈틉틋틔퇸티틱틴틸팀팁팅파팍팎판팔팜팝팟팠팡팥패팩팬팰팸팹팻팼팽퍼퍽펀펄펌펍펏펐펑페펙펜펠펨펩펫펭펴펵편펼폄폅폈평폐포폭폰폴폼폽폿퐁푀푄표푯푸푹푼풀품풉풋풍퓌퓨프픈플픔픕피픽핀필핌핍핏핑하학한할핥함합핫핬항핱해핵핸핼햄햅햇했행햐향허헉헌헐헒험헙헛헝헤헥헨헬헴헵헷헹혀혁현혈혐협혓혔형혜호혹혼홀홈홉홋홍화확환활홧황홰홱횃회획횟횡효후훅훈훌훑훔훗훠훤훨훰훼휀휑휘휙휜휠휨휩휭휴휼흄흉흐흑흔흗흘흙흠흡흣흥흩희흰흴히힉힌힐힘힙힛힝ㆁㆆㅿㆍ괵굻궉궝궹귕긇긎긏긑긔깟껭꼉꽐꾿끠맄앍앏앒얫얱옜옝웽윗윳쭌");
		AppsFlyer.trackRichEvent("af_purchase", eventValue);

		AF_Sample_BGScript.pressed ();

	}

	//On Android ou can call the conversion data directly from your CS file, or from the default AppsFlyerTrackerCallbacks
	public void didReceiveConversionData(string conversionData) {
		print ("AppsFlyerTrackerCallbacks:: got conversion data = " + conversionData);
		if (conversionData.Contains ("Non")) {
			text.GetComponent<Text> ().text = "Non-Organic Install";
		} else {
			text.GetComponent<Text> ().text = "Organic Install";
		}	
	}
	public void didReceiveConversionDataWithError(string error) {
		print ("AppsFlyerTrackerCallbacks:: got conversion data error = " + error);
	}

	public void onAppOpenAttribution(string validateResult) {
		print ("AppsFlyerTrackerCallbacks:: got onAppOpenAttribution  = " + validateResult);

	}

	public void onAppOpenAttributionFailure (string error) {
		print ("AppsFlyerTrackerCallbacks:: got onAppOpenAttributionFailure error = " + error);

	}

	//Firebase callback with token
//	public void OnTokenReceived(object sender, Firebase.Messaging.TokenReceivedEventArgs token) { 
//		AppsFlyer.updateServerUninstallToken (token.Token);
//	}



}


